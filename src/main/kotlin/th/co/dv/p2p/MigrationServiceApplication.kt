package th.co.dv.p2p

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.batch.core.Job
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.batch.*
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.context.event.*
import org.springframework.boot.SpringApplication
import org.springframework.boot.WebApplicationType
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.security.oauth2.resource.FixedAuthoritiesExtractor
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.event.*
import org.springframework.core.annotation.Order
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter
import org.springframework.security.web.firewall.HttpFirewall
import org.springframework.security.web.firewall.StrictHttpFirewall
import org.springframework.stereotype.Component
import th.co.dv.p2p.base.repository.CustomJpaRepositoryFactoryBean

import th.co.dv.p2p.customs.utilities.Condition
import th.co.dv.p2p.customs.utilities.UserAuthorization
import th.co.dv.p2p.customs.utilities.UserGroup

@SpringBootApplication
@EnableAsync
@EnableBatchProcessing
//@EnableJpaRepositories(basePackages = ["th.co.dv.p2p.base.repository"],
////		repositoryFactoryBeanClass = CustomJpaRepositoryFactoryBean::class
////)
class MigrationServiceApplication {

	companion object {
		private val logger: Logger = LoggerFactory.getLogger(this::class.java)

		@JvmStatic fun main(args: Array<String>) {
			val isJob = args.any { it.toLowerCase().contains("job") }
			logger.info("Job Arguments: $isJob")

			val web = if (isJob) {
				WebApplicationType.NONE
			} else {
				WebApplicationType.SERVLET
			}


			var context = SpringApplicationBuilder(MigrationServiceApplication::class.java)
					.web(web) // .REACTIVE, .SERVLET
					.run(*args)
			logger.info("******************* SPRING BOOT STARTED ************************")
		}
	}


	@Bean
	fun jobExecutionExitCodeGenerator(): JobExecutionExitCodeGenerator {
		return JobExecutionExitCodeGenerator()
	}

	@Autowired
	lateinit var jobExecutionExitCodeGenerator: JobExecutionExitCodeGenerator


	@Bean
	fun threadPoolTaskScheduler(): ThreadPoolTaskScheduler{
		val threadPoolTaskScheduler = ThreadPoolTaskScheduler()
		threadPoolTaskScheduler.poolSize = 5
//		threadPoolTaskScheduler.threadNamePrefix = "ThreadPoolTaskScheduler"
		return threadPoolTaskScheduler
	}
}

@Configuration
@Order(1)
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ConditionalOnProperty(name = arrayOf("spring.batch.job.enabled"), havingValue = "false")
class ResourceServerConfigurer: ResourceServerConfigurerAdapter() {

	override fun configure(http: HttpSecurity) {
		http.httpBasic().disable()

		http.authorizeRequests()
				.antMatchers("/")
				.permitAll()

	}

//	@Bean
//	fun myPrincipalExtractor(): PrincipalExtractor {
//		return MyPrincipalExtractor()
//	}
}


@Component
@ConfigurationProperties("spring.batch")
data class SpringBatchProperties(
		var job: MutableMap<String, String> = mutableMapOf()

)

///**
// * This class is used for extract user authorization from the resource server's response.
// * Example response is
// * {
// *   name: xxx,
// *   authorities: [...],
// *   userAuthentication: {
// *     details: {
// *       companies: [0100, 5260],
// *       authorizations: [{
// *         name: TAX_0105556176239,
// *         items: {
// *           Invoice: [{
// *             field: companyCode,
// *             operation: EQUAL,
// *             value=0100
// *           }, {
// *             field: companyTaxNumber,
// *             operation: EQUAL,
// *             value=0105556176239
// *           }],
// *           PurchaseOrder: [{
// *             field: companyCode,
// *             operation: EQUAL,
// *             value=0100
// *           }, {
// *             field: companyTaxNumber,
// *             operation: EQUAL,
// *             value=0105556176239
// *           }]
// *         }
// *       }, {
// *         name: COMP_0200,
// *         items: {
// *           Invoice: [{
// *             field: companyCode,
// *             operation: EQUAL,
// *             value=0200
// *           }],
// *           PurchaseOrder: [{
// *             field: companyCode,
// *             operation: EQUAL,
// *             value=0200
// *           }]
// *         }
// *       }
// *     }
// *   }
// * }
// */
//class MyPrincipalExtractor : PrincipalExtractor {
//	companion object {
//		private val logger: Logger = LoggerFactory.getLogger(this::class.java)
//	}
//
//	@Suppress("UNCHECKED_CAST")
//	override fun extractPrincipal(map: Map<String, Any>): Any {
//		logger.info("Map of the source map to extract the principal: $map")
//
//		//Extracts the authorities from the map with the a key
//		val authoritiesExtracted = FixedAuthoritiesExtractor().extractAuthorities(map).map { it.authority }
//
//		val username = map["name"] as String
//		val userAuthentication = map["userAuthentication"] as Map<*, *>?
//		val userAuthenticationDetails = userAuthentication?.let{userAuthentication["details"] as Map<*, *>?}
//
//		println("============================== username $username")
//		println("============================== userAuthentication $userAuthentication")
//		println("============================== userAuthenticationDetails $userAuthenticationDetails")
//
//		val authorization = userAuthenticationDetails?.let{userAuthenticationDetails["authorizations"] as List<Map<String, Any>>?}
//		val groups = authorization?.map { group ->
//			UserGroup(
//					name = group["name"]!!.toString(),
//					states = (group["states"] as Map<String, Any>).mapValues { (_, value) ->
//						(value as List<Map<String, String>>).map {
//							Condition(
//									field = it["field"]!!,
//									operator = it["operation"]!!,
//									value = it["value"]!!
//							)
//						}
//					})
//		}
//
//		val userAuthorization = UserAuthorization(
//				username = username,
//				authorities = authoritiesExtracted,
//				userGroups = groups?: emptyList())
//
//		logger.info("Current userAuthorization: $userAuthorization")
//
//		return userAuthorization
//	}
//}