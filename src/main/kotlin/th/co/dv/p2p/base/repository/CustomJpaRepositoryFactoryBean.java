package th.co.dv.p2p.base.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

import javax.persistence.EntityManager;
import java.io.Serializable;


/**
 * @author chatchch
 */
public class CustomJpaRepositoryFactoryBean<R extends JpaRepository<T, ID>, T, ID extends Serializable> extends JpaRepositoryFactoryBean<R, T, ID>
{
	public CustomJpaRepositoryFactoryBean(Class<R> repositoryInterface) {
		super(repositoryInterface);
	}

	protected RepositoryFactorySupport createRepositoryFactory(EntityManager entityManager)
	{

		return new MyRepositoryFactory(entityManager);
	}

	private static class MyRepositoryFactory<T, I extends Serializable> extends JpaRepositoryFactory
	{
		protected Logger logger = LoggerFactory.getLogger(getClass());

		private EntityManager entityManager;

		public MyRepositoryFactory( EntityManager entityManager )
		{
			super(entityManager);

			this.entityManager = entityManager;
		}

//		protected Object getTargetRepository(RepositoryInformation information)
//		{
//				if (CustomJpaRepository.class.isAssignableFrom(information.getRepositoryInterface())) {
//					logger.debug("Creating CustomJpaRepository for " + information.getDomainType());
//					return new CustomJpaRepositoryImpl<T, I>((Class<T>) information.getDomainType(), entityManager);
//				}
//				return super.getTargetRepository(information);
//		}
//
//		protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata)
//		{
//				if (CustomJpaRepository.class.isAssignableFrom(metadata.getRepositoryInterface())) {
//						return CustomJpaRepository.class;
//				}
//				return super.getRepositoryBaseClass(metadata);
//		}

	}

}
