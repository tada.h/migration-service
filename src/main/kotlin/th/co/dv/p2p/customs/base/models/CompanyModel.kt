package th.co.dv.p2p.customs.base.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class CompanyModel (

        @JsonProperty("code")
        val code: String? = null,

        @JsonProperty("name1")
        val name1: String? = null,

        @JsonProperty("name2")
        val name2: String? = null,

        @JsonProperty("street")
        val street: String? = null,

        @JsonProperty("district")
        val district: String? = null,

        @JsonProperty("city")
        val city: String? = null,

        @JsonProperty("postalCode")
        val postalCode: String? = null,

        @JsonProperty("taxId")
        val taxId: String? = null,

        @JsonProperty("businessPlace")
        val businessPlace: String? = null,

        @JsonProperty("businessPlaceName1")
        val businessPlaceName1: String? = null,

        @JsonProperty("accountNo")
        val accountNo: String? = null,

        @JsonProperty("bankCode")
        val bankCode: String? = null

){

}