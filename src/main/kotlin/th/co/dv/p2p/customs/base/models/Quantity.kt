package th.co.dv.p2p.customs.base.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal

/**
 * In the future we might want to enforce the allowable unit type in Quantity
 * for now its just of type string of any
 */
enum class Unit {
    KILOGRAM,
    GRAM,
    TON,
    BOX,
    PC,
    EA
}

/**
 * Quantity represents a positive quantity of some unit (kilograms, boxes, etc.)
 * The nominal quantity represented by each individual Quantity is equal to the 3

 *
 * @property initial the number of units promised during parent state issuance as a bigdecimal varue.
 * @property consumed the number of units issued during children state issuance as a bigdecimal varue.
 * @property unit the type of the token, for example [Kilograms, grams].
 * @property remaining the balance by taking initial minus consumed.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class Quantity(

        @JsonProperty("initial")
        var initial: BigDecimal,
        @JsonProperty("consumed")
        var consumed: BigDecimal = BigDecimal.ZERO.setScale(3),
        @JsonProperty("unit")
        var unit: String,
        @JsonProperty("remaining")
        var remaining: BigDecimal = initial) {

    constructor() : this(
            initial = BigDecimal.ZERO,
            consumed = BigDecimal.ZERO,
            unit = "",
            remaining = BigDecimal.ZERO
    )

    constructor(initial: BigDecimal, consumed: BigDecimal, unit: String) : this(
            initial = initial.scaleUp(3),
            consumed = consumed.scaleUp(3),
            unit = unit,
            remaining = initial.minus(consumed).scaleUp(3))

    constructor(initial: BigDecimal, unit: String) : this(
            initial = initial.scaleUp(3),
            consumed = BigDecimal.ZERO.setScale(3),
            unit = unit,
            remaining = initial.scaleUp(3))

    constructor(initial: Double, unit: String) : this(
            initial = initial.toDecimal().scaleUp(3),
            consumed = BigDecimal.ZERO.setScale(3),
            unit = unit,
            remaining = initial.toDecimal().scaleUp(3))

    constructor(initial: Double, consumed: Double, unit: String) : this(
            initial = initial.toDecimal().scaleUp(3),
            consumed = consumed.toDecimal().scaleUp(3),
            unit = unit,
            remaining = initial.toDecimal().minus(BigDecimal(consumed)).scaleUp(3))

}

fun BigDecimal.scaleUp(newScale: Int) : BigDecimal = this.setScale(newScale, BigDecimal.ROUND_HALF_UP)!!
fun Double.toDecimal(): BigDecimal = BigDecimal(this.toString())
