package th.co.dv.p2p.customs.model

import java.math.BigDecimal
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "credit_note_item_r6")
data class CreditNoteItemR6(

        @Id
        var linearId: String ? = null,
        var buyer: String? = null,
        var seller: String? = null,
        var accounting: String? = null,
        var bank: String? = null,
        var externalId: String? = null,
        var purchaseItemLinearId: String? = null,
        var invoiceItemLinearId: String? = null,
        var invoiceItemExternalId: String? = null,
        var creditNoteLinearId: String? = null,
        var materialNumber: String? = null,
        var materialDescription: String? = null,
        var quantity: BigDecimal? = null,
        var site: String? = null,
        var siteDescription: String? = null,
        var section: String? = null,
        var sectionDescription: String? = null,
        var referenceField1: String? = null,
        var referenceField2: String? = null,
        var referenceField3: String? = null,
        var referenceField4: String? = null,
        var referenceField5: String? = null,
        var unit: String? = null,
        var unitDescription: String? = null,
        var unitPrice: Long? = null,
        var subTotal: Long? = null,
        var taxRate: BigDecimal? = null,
        var vatTotal: Long? = null,
        var withholdingTaxRate: BigDecimal? = null,
        var withholdingTaxFormType: String? = null,
        var withholdingTaxPayType: String? = null,
        var withholdingTaxRemark: String? = null,
        var withholdingTaxIncomeType: String? = null,
        var withholdingTaxIncomeDescription: String? = null,
        var withholdingTaxCode: String? = null,
        var currency: String? = null,
        var unmatchedCode: String? = null,
        var unmatchedReason: String? = null,
        var lastMatchUpdatedDate: Date? = null,
        var issuedDate: Date? = null,
        var lifecycle: String? = null,
        var lifecycleStage: Long? = null,
        var customisedFields: String? = null,
        var customisedFieldsUpdatedDate: Date? = null

)