package th.co.dv.p2p.customs.base.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class PaymentModel(
        @JsonProperty("linearId")
        var linearId: String? = null,
        @JsonProperty("externalId")
        var externalId: String? = null,
        @JsonProperty("buyerCompanyCode")
        var buyerCompanyCode: String? = null,
        @JsonProperty("buyerBankCode")
        var buyerBankCode: String? = null,
        @JsonProperty("buyerBankAccountNumber")
        var buyerBankAccountNumber: String? = null,
        @JsonProperty("paymentDate")
        var paymentDate: String? = null,
        @JsonProperty("issuedDate")
        var issuedDate: String? = null,
        @JsonProperty("paymentSystem")
        var paymentSystem: String? = null,
        @JsonProperty("feeChargeTo")
        var feeChargeTo: String? = null,
        @JsonProperty("totalPaymentAmount")
        var totalPaymentAmount: String? = null,
        @JsonProperty("paymentStatus")
        var paymentStatus: String? = null,
        @JsonProperty("paymentMessage")
        var paymentMessage: String? = null,
        @JsonProperty("lifecycle")
        var lifecycle: String? = null,
        @JsonProperty("buyer")
        var buyer: PartyModel? = null,
        @JsonProperty("bank")
        var bank: PartyModel? = null,
        @JsonProperty("paymentItemModels")
        var paymentItemModels: List<PaymentItemModel> = emptyList()
        )
