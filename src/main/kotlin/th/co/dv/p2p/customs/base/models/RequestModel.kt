package th.co.dv.p2p.customs.base.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import org.codehaus.jackson.map.annotate.JsonSerialize
import java.math.BigDecimal

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonSerialize
data class RequestModel(
        val buyer: PartyModel? = null,
        val seller: PartyModel? = null,
        val bank: PartyModel? = null,

        val companyCode: String? = null,
        val companyName: String? = null,
        val companyTaxNumber: String? = null,
        val companyBranchCode: String? = null,
        val companyBranchName: String? = null,
        val companyAddress: String? = null,
        val companyTelephone: String? = null,

        val vendorNumber: String? = null,
        val vendorBranchCode: String? = null,
        val vendorBranchName: String? = null,
        val vendorName: String? = null,
        val vendorTaxNumber: String? = null,
        val vendorAddress: String? = null,
        val vendorTelephone: String? = null,

        val issuedDate: String? = null,
        val externalId: String? = null,
        val subTotal: BigDecimal? = null,
        val vatTotal: BigDecimal? = null,
        val total: BigDecimal? = null,
        val withholdingTaxAmount: BigDecimal? = null,
        val currency: String? = null,

        val createdBy: String? = null,
        val lastEditedDate: String? = null,
        val lastEditedBy: String? = null,

        val linearId: String? = null,
        val lifecycle: String? = null,
        val status: String? = null,
        val requestItems: List<RequestItemModel> = emptyList(),

        val referenceField1: String? = null,
        val referenceField2: String? = null,
        val referenceField3: String? = null,
        val referenceField4: String? = null,
        val referenceField5: String? = null,
        val customisedFields: Map<String, Any> = emptyMap(),
        val customisedFieldsUpdatedDate: String? = null,

        val type: String? = null,
        val subType: String? = null,
        val referenceType: String? = null,
        val referenceNumber: String? = null,
        val referenceLinearId: String? = null,
        val requestReason: String? = null,
        val requestAttachment: List<FileAttachmentModel>? = emptyList(),

        val agreedBy: String? = null,
        val agreedDate: String? = null,
        val agreedRemark: String? = null,
        val agreementFlag: Boolean? = null,

        val documentNumber: String? = null,
        val documentType: String? = null,
        val documentDate: String? = null,
        val paymentDueDate: String? = null,
        val documentReason: String? = null,
        val documentAttachment: List<FileAttachmentModel>? = emptyList(),

        val clarifiedRemark: String? = null,
        val clarifiedBy: String? = null,
        val clarifiedDate: String? = null,

        val cancelledRemark: String? = null,
        val cancelledBy: String? = null,
        val cancelledDate: String? = null,

        @get:JsonProperty("isReadyToConvert")
        val isReadyToConvert: Boolean? = null
) {
    fun isReadyToConvert() = isReadyToConvert == true
}
