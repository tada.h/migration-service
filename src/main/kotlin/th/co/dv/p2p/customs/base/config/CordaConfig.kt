package th.co.dv.p2p.customs.base.config

import org.springframework.stereotype.Component
import org.springframework.boot.context.properties.ConfigurationProperties

@Component
@ConfigurationProperties("corda")
data class CordaConfig(
        var endpoint: String? = null,
        var clientID: String? = null,
        var clientSecret: String? = null,
        var accessTokenUri: String? = null,
        var interfacePath: MutableMap<String, String> = mutableMapOf(),
        var timeout: Int = 3000 * 1000
)