package th.co.dv.p2p.customs.model

import java.math.BigDecimal
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "goods_received_item_r9")
class GoodsReceivedItemR9(

        @Column(name = "buyer")
        var buyer: String? = null,

        @Column(name = "seller")
        var seller: String? = null,

        @Column(name = "accounting")
        var accounting: String? = null,

        @Column(name = "purchase_order_external_id")
        var purchaseOrderExternalId: String? = null,

        @Column(name = "purchase_item_external_id")
        var purchaseItemExternalId: String? = null,

        @Column(name = "purchase_item_linear_id")
        var purchaseItemLinearId: String? = null,

        // Hibernate function to convert to lowercase in SQL schema.
        // The BLOB stored by corda core is still in original format.
        // For 3WM/2WM purposes, we use this column to compare against a lowered cased operand
        // i.e column.externalId equals lowercase(input)
        @Column(name = "invoice_external_id")
        var invoiceExternalId: String? = null,

        @Column(name = "invoice_external_id_lowercase")
//        @ColumnTransformer(write = "lower(?)")
        var invoiceExternalIdLowerCase: String? = null,

        @Column(name = "initial_invoice_external_id")
        var initialInvoiceExternalId: String? = null,

        @Column(name = "initial_invoice_external_id_lowercase")
//        @ColumnTransformer(write = "lower(?)")
        var initialInvoiceExternalIdLowerCase: String? = null,

        @Column(name = "sibling_linear_id")
        var siblingLinearId: String? = null,

        @Column(name = "delivery_note_external_id")
        var deliveryNoteExternalId: String? = null,

        @Column(name = "external_id")
        var externalId: String? = null,

        @Column(name = "goods_received_item_number")
        var goodsReceivedExternalId: String? = null,

        @Column(name = "movement_type")
        var movementType: String? = null,

        @Column(name = "movement_class")
        var movementClass: String? = null,

        @Column(name = "company_code")
        var companyCode: String? = null,

        @Column(name = "vendor_number")
        var vendorNumber: String? = null,

        @Column(name = "vendor_name")
        var vendorName: String? = null,

        @Column(name = "vendor_branch_code")
        var vendorBranchCode: String? = null,

        @Column(name = "material_number")
        var materialNumber: String? = null,

        @Column(name = "material_group")
        var materialGroup: String? = null,

        @Column(name = "material_description", columnDefinition = "TEXT")
        var materialDescription: String? = null,

        @Column(name = "quantity_initial", precision = 19, scale = 3)
        var quantityInitial: BigDecimal? = null,

        @Column(name = "quantity_unit")
        var quantityUnit: String? = null,

        @Column(name = "document_entry_year")
        var documentEntryYear: String? = null,

        @Column(name = "document_entry_date")
        var documentEntryDate: Date? = null,

        @Column(name = "posting_date")
        var postingDate: Date? = null,

        @Column(name = "created_by")
        var createdBy: String? = null,

        @Column(name = "last_edited_by")
        var lastEditedBy: String? = null,

        @Column(name = "last_edited_date")
        var lastEditedDate: Date? = null,

        @Column(name = "initial_goods_received_item_linear_id")
        var initialGoodsReceivedItemLinearId: String? = null,

        @Column(name = "lifecycle")
        var lifecycle: String? = null,

        @Column(name = "linear_id")
        var linearId: String? = null,

        @Column(name = "unit_description")
        var unitDescription: String? = null,

        @Column(name = "accounting_number")
        var accountingNumber: String? = null,

        @Column(name = "delivery_complete_flag")
        var deliveryCompleteFlag: String? = null,

        @Column(name = "goods_received_pre_inspection_year")
        val goodsReceivedPreInspectionYear: String? = null,

        @Column(name = "goods_received_external_id_pre_inspection")
        val goodsReceivedExternalIdPreInspection: String? = null,

        @Column(name = "external_id_pre_inspection")
        val externalIdPreInspection: String? = null,

        @Column(name = "customised_fields", nullable = false)
//        @Type(type = "org.hibernate.type.ImageType")
        var customisedFields: String? = null,

        @Column(name = "customised_fields_updated_date")
        var customisedFieldsUpdatedDate: Date? = null,

        @Column(name = "issued_date")
        val issuedDate: Date? = null,

        @Column(name = "last_tagged_by")
        val lastTaggedBy: String? = null,

        @Column(name = "last_tagged_date")
        val lastTaggedDate: Date? = null,

        //New field
        @Column(name = "goods_received_linear_id")
        var goodsReceivedLinearId: String? = null,

        @Column(name = "company_branch_code")
        var companyBranchCode: String? = null,

        @Column(name = "company_name")
        var companyName: String? = null,

        @Column(name = "company_tax_number")
        var companyTaxNumber: String? = null,

        @Column(name = "initial_document_entry_year")
        var initialDocumentEntryYear: String? = null,

        @Column(name = "initial_external_id")
        var initialExternalId: String? = null,

        @Column(name = "initial_goods_received_external_id")
        var initialGoodsReceivedExternalId: String? = null,

        @Column(name = "vendor_tax_number")
        var vendorTaxNumber: String? = null,

        @Column(name = "site")
        var site: String? = null,

        @Column(name = "site_description", columnDefinition = "TEXT")
        var siteDescription: String? = null,

        @Column(name = "section")
        var section: String? = null,

        @Column(name = "section_description", columnDefinition = "TEXT")
        var sectionDescription: String? = null,

        @Column(name = "unit_price")
        var unitPrice: Long? = null,

        @Column(name = "reference_field_1")
        val referenceField1: String? = null,

        @Column(name = "reference_field_2")
        val referenceField2: String? = null,

        @Column(name = "reference_field_3")
        val referenceField3: String? = null,

        @Column(name = "reference_field_4")
        val referenceField4: String? = null,

        @Column(name = "reference_field_5")
        val referenceField5: String? = null,

        @Column(name = "currency", length = 3)
        var currency: String? = null

)
