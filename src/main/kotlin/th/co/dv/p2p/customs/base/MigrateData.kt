package th.co.dv.p2p.customs.base


interface MigrateData{
    val equalFields : List<String>
    val newFields : List<String>
    val zero2DigitsFields : List<String>
    val zero3DigitsFields : List<String>
}
