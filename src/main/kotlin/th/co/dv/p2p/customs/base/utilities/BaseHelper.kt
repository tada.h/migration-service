package th.co.dv.p2p.customs.base.utilities

import org.slf4j.Logger
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*

import th.co.dv.p2p.customs.base.Amount
import java.lang.IllegalStateException

val String.CURRENCY: Currency get() = Currency.getInstance(this)

// TODO: Stop using Double functions and use Big Decimal for arithmatic functions
fun Double.scaleToPenny(tokenSize: Int = 2): Long = this.toDecimal().movePointRight(tokenSize).setScale(0, BigDecimal.ROUND_HALF_UP).toLong()

fun Double.scaleUp(newScale: Int): Double = BigDecimal(this.toString()).setScale(newScale, BigDecimal.ROUND_HALF_UP).toDouble()
fun Double.scaleDown(newScale: Int): Double = BigDecimal(this.toString()).setScale(newScale, BigDecimal.ROUND_HALF_DOWN).toDouble()
fun Double.multiply(another: Double): Double = BigDecimal(this.toString()).multiply(BigDecimal(another.toString())).setScale(2, BigDecimal.ROUND_HALF_UP).toDouble()

/** Currency helpers. */
fun Long.toDecimal(tokenSize: Int = 2): BigDecimal = BigDecimal(this.toString()).movePointLeft(tokenSize)

fun Double.toDecimal(): BigDecimal = BigDecimal(this.toString())
fun BigDecimal.scaleToPenny(tokenSize: Int = 2): Long = (this.movePointRight(tokenSize)).scaleUp(0).toLong()
fun BigDecimal.scaleUp(newScale: Int): BigDecimal = this.setScale(newScale, BigDecimal.ROUND_HALF_UP)!!
fun BigDecimal.scaleDown(newScale: Int): BigDecimal = this.setScale(newScale, BigDecimal.ROUND_HALF_DOWN)!!

fun BigDecimal.isEqual(other: BigDecimal): Boolean = this.compareTo(other) == 0
fun BigDecimal.isNotEqual(other: BigDecimal): Boolean = this.compareTo(other) != 0
fun BigDecimal.isGreaterThan(other: BigDecimal): Boolean = this.compareTo(other) == 1
fun BigDecimal.isLessThan(other: BigDecimal): Boolean = this.compareTo(other) == -1
fun BigDecimal.isLessOrEqual(other: BigDecimal): Boolean = this.isLessThan(other) || this.isEqual(other)
fun BigDecimal.isGreaterOrEqual(other: BigDecimal): Boolean = this.isGreaterThan(other) || this.isEqual(other)
fun BigDecimal.toAmount(currency: String): Amount<Currency> = Amount.fromDecimal(this.scaleUp(currency.CURRENCY.defaultFractionDigits), currency.CURRENCY)

/**
 * Returns the sum of all values produced by [selector] function applied to each element in
 * the collection.
 */
inline fun <T> Iterable<T>.sumByDecimal(selector: (T) -> BigDecimal): BigDecimal {
    var sum: BigDecimal = BigDecimal.ZERO
    for (element in this) {
        sum += selector(element)
    }
    return sum
}

inline fun <T> Iterable<T>.sumByLong(selector: (T) -> Long): Long {
    var sum: Long = 0L
    for (element in this) {
        sum += selector(element)
    }
    return sum
}

fun Instant.plusDays(value: Long): Instant {
    return this.plus(value, ChronoUnit.DAYS)
}

fun Instant.minusDays(value: Long): Instant {
    return this.minus(value, ChronoUnit.DAYS)
}

/**
 * Returns the number of days by performing [secondDate] - [firstDate]
 * i.e 10-Jan 12am minus 4-Jan 7am = 5.7 days = 5 days
 */
fun daysInBetween(firstDate: Instant, secondDate: Instant): Long {
    return ChronoUnit.DAYS.between(firstDate, secondDate)
}

/**
 * Returns a silent try on the action that catch and mute the exception in log.
 * WARNING:
 * 1. MUST be inlined, else calling subflow in the lambda will throw error
 * 2. MUST be non-@Suspendable, else quasar will throw verifyError due to SilentEvent data class
 */
inline fun <T> trySilently(logger: Logger? = null, body: () -> T): SilentEvent<T> {

    return try {
        val event = body()
        SilentEvent(body = event)
    } catch (ex: Exception) {
        println("trySilently terminated by unexpected exception ${ex.message}")
        logger?.error("trySilently exception", ex)
        SilentEvent(exception = ex)
    }
}

data class SilentEvent<out T>(
        val body: T? = null,
        val exception : Throwable? = null)


object Conditions {

    @Suppress("NOTHING_TO_INLINE")   // Inlining this takes it out of our committed ABI.
    inline infix fun String.using(expr: Boolean) {
        if (!expr) throw IllegalStateException("Failed requirement: $this")
    }

    class IllegalStateException(msg: String) :IllegalArgumentException("$msg")
}