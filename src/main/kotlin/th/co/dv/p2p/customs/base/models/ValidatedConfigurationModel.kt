package th.co.dv.p2p.customs.base.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.math.BigDecimal


/**
 * Returned when get invoice/credit note configuration from offchain
 * A ValidatedConfigurationModel contains:
 *  1) [minimumDocumentEffectiveDate]: The minimum back date that we allow to add in invoice/credit note date
 *  2) [maximumDocumentEffectiveDate]: The maximum back date that we allow to add in invoice/credit note date
 *  2) [minimumDocumentExpiryInDays]: The minimum threshold of document expiry in days i.e 3.0 days
 *  3) [maximumDocumentExpiryInDays]: The maximum threshold of document expiry in days i.e 3.0 days
 *  4) [attachmentConfiguration]: The sets of allowed file format configred for each type of documents, i.e Invoice.Receipt : JPG,PDF,TIF
 *  5) [invoiceFinancingIsAllowed]: Check if provided vendor is allowed invoice financing
 */
@JsonIgnoreProperties(ignoreUnknown = true)
data class ValidatedConfigurationModel(
        val invoiceFinancingIsAllowed: Boolean? = null,
        val minimumDocumentEffectiveDate: String? = null,
        val maximumDocumentEffectiveDate: String? = null,
        val minimumDocumentExpiryInDays: BigDecimal? = null,
        val maximumDocumentExpiryInDays: BigDecimal? = null,
        val attachmentConfiguration: List<AttachmentConfigurationModel>? = null
)
