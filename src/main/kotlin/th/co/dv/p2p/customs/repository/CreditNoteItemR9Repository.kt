package th.co.dv.p2p.customs.repository

import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import th.co.dv.p2p.customs.model.CreditNoteItemR9

interface CreditNoteItemR9Repository : PagingAndSortingRepository<CreditNoteItemR9, Long>, JpaSpecificationExecutor<CreditNoteItemR9> {
    fun findByExternalIdIn(externalIds : List<String>): List<CreditNoteItemR9>
}
