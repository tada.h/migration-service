package th.co.dv.p2p.customs.service.corda

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.map.annotate.JsonSerialize
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.core.ParameterizedTypeReference
import org.springframework.core.io.Resource
import org.springframework.http.*
import org.springframework.http.client.ClientHttpRequestExecution
import org.springframework.http.client.ClientHttpRequestInterceptor
import org.springframework.http.client.ClientHttpResponse
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext
import org.springframework.security.oauth2.client.OAuth2RestTemplate
import org.springframework.security.oauth2.client.token.AccessTokenProviderChain
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsAccessTokenProvider
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.ResponseErrorHandler
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder
import th.co.dv.p2p.customs.base.config.CordaConfig
import th.co.dv.p2p.customs.base.models.*
import th.co.dv.p2p.customs.base.models.P2PModel.PageModel
import th.co.dv.p2p.customs.model.ResponseModel
import th.co.dv.p2p.customs.scg.model.ResponseModelWithType
import java.math.BigDecimal
import java.nio.charset.Charset
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter


data class ResponseEntityModel<T>(
        val headers: Any? = null,
        val body: T? = null,
        val statusCode: Any? = null,
        val statusCodeValue: Any? = null
)

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@Service
class CordaService {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(CordaService::class.java)
    }

    @Autowired
    lateinit var properties: CordaConfig

//    fun queryPurchaseOrder(): PageModel<Any> {
//        val template = getRestTemplate()
//        template.errorHandler = ApiErrorHandler()
//        return template.exchange(
//                properties.endpoint + properties.interfacePath["invoice"],
//                org.springframework.http.HttpMethod.GET,
//                null,
//                object : ParameterizedTypeReference<ResponseModelWithType<PageModel<Any>>>() {}
//        ).body?.data!!
//    }

    fun getRestTemplate(): RestTemplate {
        var restTemplate: RestTemplate = buildOauthRestTemplate()
        var intercepts: List<ClientHttpRequestInterceptor> = listOf(RequestResponseLoggingInterceptor())
        restTemplate.interceptors = intercepts
        return restTemplate
    }

    private fun buildOauthRestTemplate(): OAuth2RestTemplate {
        val details = ClientCredentialsResourceDetails()
        details.clientId = properties.clientID
        details.clientSecret = properties.clientSecret
        details.accessTokenUri = properties.accessTokenUri

        val clientContext = DefaultOAuth2ClientContext()
        val template = OAuth2RestTemplate(details, clientContext)
        val accessTokenProvider = AccessTokenProviderChain(
                listOf(
                        ClientCredentialsAccessTokenProvider()
                )
        )
        template.setAccessTokenProvider(accessTokenProvider)

        val factory: SimpleClientHttpRequestFactory = template.requestFactory as SimpleClientHttpRequestFactory
        factory.setReadTimeout(properties.timeout)
        factory.setConnectTimeout(properties.timeout)

        return template
    }


}

inline fun <reified T : Any> getDataResponse(response: ResponseModel): T {
    val dataString = jacksonObjectMapper().writeValueAsString(response.data)
    return jacksonObjectMapper().readValue(dataString)
}

class ApiErrorHandler : ResponseErrorHandler {

    override fun handleError(response: ClientHttpResponse) {
    }

    override fun hasError(response: ClientHttpResponse): Boolean {
        return response.statusCode != HttpStatus.CREATED
    }
}

class RequestResponseLoggingInterceptor : ClientHttpRequestInterceptor {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(RequestResponseLoggingInterceptor::class.java)
    }

    override fun intercept(request: HttpRequest, body: ByteArray, execution: ClientHttpRequestExecution): ClientHttpResponse {
        logRequest(request, body)
        val response = execution.execute(request, body)
        logResponse(response)
        return response
    }

    private fun logRequest(request: HttpRequest, body: ByteArray) {
        logger.info("Request URL : ${request.uri}")
        logger.info("Request Method : ${request.method}")
        logger.info("Request Header : ${request.headers}")
        logger.info("Request Body : ${String(body, Charset.forName("UTF-8"))}")
    }

    private fun logResponse(response: ClientHttpResponse) {
        logger.info("Response Raw Status Code : ${response.rawStatusCode}")
        logger.info("Response Status Code : ${response.statusCode}")
        logger.info("Response Status Text: ${response.statusText}")
        logger.info("Response Headers: ${response.headers}")
    }
}
