package th.co.dv.p2p.customs.base.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import java.math.BigDecimal

/**
 * Data model class that is passed in from the Front End that will be translated into Corda State
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class CreditNoteItemModel(
        val linearId: String? = null,
        val buyer: PartyModel? = null,
        val seller: PartyModel? = null,
        val accounting: PartyModel? = null,
        val bank: PartyModel? = null,
        val externalId: String? = null,
        val purchaseItemLinearId : String? = null,
        val invoiceItemLinearId: String? = null,
        val invoiceItemExternalId: String? = null,
        val creditNoteLinearId: String? = null,
        val materialNumber: String? = null,
        val materialDescription: String? = null,
        val quantity: BigDecimal? = null,
        val unit: String? = null,
        val unitDescription: String? = null,
        val unitPrice: BigDecimal? = null,
        val subTotal: BigDecimal? = null,
        val taxRate: BigDecimal? = null,
        val vatTotal: BigDecimal? = null,
        val unmatchedCode: List<String>? = null,
        val unmatchedReason: String? = null,
        val serviceDescription: String? = null,
        val currency: String? = null,
        val issuedDate: String? = null,
        val lifecycle: String? = null,
        val invoiceItems: List<InvoiceItemModel>? = null,
        val purchaseItem: PurchaseItemModel? = null,
        val goodsReceivedItems: List<GoodsReceivedItemModel> = emptyList(),
        val customisedFields: Map<String, Any> = emptyMap(),
        val customisedFieldsUpdatedDate: String? = null,
        val status: String? = null,
        val buyerApprovedDate: String? = null,
        val buyerApprovedRemark: String? = null,
        val buyerApprovedUser: String? = null,
        val buyerRejectedDate: String? = null,
        val buyerRejectedRemark: String? = null,
        val buyerRejectedUser: String? = null,
        val withholdingTaxCode: String? = null,
        val withholdingTaxRate: BigDecimal? = null,
        var requestItemExternalId: String? = null,
        var requestItemLinearId: String? = null
) {
}