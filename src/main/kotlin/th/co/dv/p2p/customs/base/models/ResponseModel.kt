package th.co.dv.p2p.customs.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.nimbusds.oauth2.sdk.ErrorObject
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class ResponseModel(

        @JsonProperty("statusCode")
        val statusCode: Int? = null,
        @JsonProperty("message")
        val message: String? = null,
        @JsonProperty("data")
        val data: Any? = null,
        @JsonProperty("error")
        val error: ErrorObject? = null
)

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "This is a standard response structure with specific data type that's api return to the client.")
data class ResponseModelSpecific<T>(
        @ApiModelProperty(value = "Http status code [200 = success, 500 = fail]", example = "200")
        val statusCode: String? = null,
        @ApiModelProperty(value = "Response message normally in case success we send \"Success\" case fail we send {Exception message}.", example = "Success")
        val message: String? = null,
        @ApiModelProperty(value = "The data that api return to client (data type is depend on data that we return)")
        val data: T? = null,
        @ApiModelProperty(value = "The error object that api return to client")
        val error: ErrorObject? = null)

enum class ErrorClassType(val code: String) {
        CODE("001"),
        DATABASE("002")
}

class IllegalDataException(msg: String) :IllegalArgumentException("$msg")
