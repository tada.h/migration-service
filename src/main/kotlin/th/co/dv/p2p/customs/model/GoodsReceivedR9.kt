package th.co.dv.p2p.customs.model

import java.math.BigDecimal
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "goods_received_item_r9")
class GoodsReceivedR9(

        @Column(name = "accounting")
        var accounting: String? = null,

        @Column(name = "advance_deductible_amount")
        var advanceDeductibleAmount: Long? = null,

        @Column(name = "buyer")
        var buyer: String? = null,

        @Column(name = "company_code")
        var companyCode: String? = null,

        @Column(name = "company_branch_code")
        var companyBranchCode: String? = null,

        @Column(name = "company_name")
        var companyName: String? = null,

        @Column(name = "company_tax_number")
        var companyTaxNumber: String? = null,

        @Column(name = "company_address_1")
        var companyAddress1: String? = null,

        @Column(name = "company_address_2")
        var companyAddress2: String? = null,

        @Column(name = "company_branch_name")
        var companyBranchName: String? = null,

        @Column(name = "company_city")
        var companyCity: String? = null,

        @Column(name = "company_country")
        var companyCountry: String? = null,

        @Column(name = "company_department")
        var companyDepartment: String? = null,

        @Column(name = "company_district")
        var companyDistrict: String? = null,

        @Column(name = "company_officer_email")
        var companyOfficerEmail: String? = null,

        @Column(name = "company_officer_name")
        var companyOfficerName: String? = null,

        @Column(name = "company_officer_telephone")
        var companyOfficerTelephone: String? = null,

        @Column(name = "company_organization")
        var companyOrganization: String? = null,

        @Column(name = "company_postal_code")
        var companyPostalCode: String? = null,

        @Column(name = "company_telephone")
        var companyTelephone: String? = null,

        @Column(name = "company_email")
        var companyEmail: String? = null,

        @Column(name = "created_by")
        var createdBy: String? = null,

        @Column(name = "last_edited_by")
        var lastEditedBy: String? = null,

        @Column(name = "last_edited_date")
        var lastEditedDate: Date? = null,

        //@Lob
        @Column(name = "customised_fields", nullable = false)
        //@Type(type = "corda-wrapper-binary")
//        @Type(type = "org.hibernate.type.ImageType")
        var customisedFields: String? = null,

        @Column(name = "customised_fields_updated_date")
        var customisedFieldsUpdatedDate: Date? = null,

        @Column(name = "delivery_note_external_id")
        var deliveryNoteExternalId: String? = null,

        @Column(name = "document_entry_date")
        var documentEntryDate: Date? = null,

        @Column(name = "document_entry_year")
        var documentEntryYear: String? = null,

        @Column(name = "external_id")
        var externalId: String? = null,

        @Column(name = "invoice_external_id")
        var invoiceExternalId: String? = null,

        @Column(name = "invoice_external_id_lowercase")
//        @ColumnTransformer(write = "lower(?)")
        var invoiceExternalIdLowerCase: String? = null,

        @Column(name = "initial_invoice_external_id")
        var initialInvoiceExternalId: String? = null,

        @Column(name = "initial_invoice_external_id_lowercase")
//        @ColumnTransformer(write = "lower(?)")
        var initialInvoiceExternalIdLowerCase: String? = null,

        @Column(name = "issued_date")
        var issuedDate: Date? = null,

        @Column(name = "lifecycle")
        var lifecycle: String? = null,

        @Column(name = "linear_id")
        var linearId: String? = null,

        @Column(name = "payment_due_date")
        var paymentDueDate: Date? = null,

        @Column(name = "posting_date")
        var postingDate: Date? = null,

        @Column(name = "reference_field_1")
        var referenceField1: String? = null,

        @Column(name = "reference_field_2")
        var referenceField2: String? = null,

        @Column(name = "reference_field_3")
        var referenceField3: String? = null,

        @Column(name = "reference_field_4")
        var referenceField4: String? = null,

        @Column(name = "reference_field_5")
        var referenceField5: String? = null,

        @Column(name = "retention_deductible_amount")
        var retentionDeductibleAmount: Long? = null,

        @Column(name = "seller")
        var seller: String? = null,

        @Column(name = "vendor_branch_code")
        var vendorBranchCode: String? = null,

        @Column(name = "vendor_branch_name")
        var vendorBranchName: String? = null,

        @Column(name = "vendor_name")
        var vendorName: String? = null,

        @Column(name = "vendor_name_lowercase")
        var vendorNameLowerCase: String? = null,

        @Column(name = "vendor_officer_email")
        var vendorOfficerEmail: String? = null,

        @Column(name = "vendor_officer_telephone")
        var vendorOfficerTelephone: String? = null,

        @Column(name = "vendor_organization")
        var vendorOrganization: String? = null,

        @Column(name = "vendor_officer_name")
        var vendorOfficerName: String? = null,

        @Column(name = "vendor_number")
        var vendorNumber: String? = null,

        @Column(name = "vendor_address_1")
        var vendorAddress1: String? = null,

        @Column(name = "vendor_address_2")
        var vendorAddress2: String? = null,

        @Column(name = "vendor_city")
        var vendorCity: String? = null,

        @Column(name = "vendor_country")
        var vendorCountry: String? = null,

        @Column(name = "vendor_district")
        var vendorDistrict: String? = null,

        @Column(name = "vendor_department")
        var vendorDepartment: String? = null,

        @Column(name = "vendor_email")
        var vendorEmail: String? = null,

        @Column(name = "vendor_postal_code")
        var vendorPostalCode: String? = null,

        @Column(name = "vendor_telephone")
        var vendorTelephone: String? = null,

        @Column(name = "vendor_tax_number")
        var vendorTaxNumber: String? = null

//        @OneToMany(fetch = FetchType.LAZY, cascade = arrayOf(CascadeType.PERSIST))
//        @JoinColumns(
//                JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id"),
//                JoinColumn(name = "output_index", referencedColumnName = "output_index"))
//        @OrderColumn
//        var goodsReceivedAttachment: List<GoodsReceivedAttachmentEntity> = listOf()
)
