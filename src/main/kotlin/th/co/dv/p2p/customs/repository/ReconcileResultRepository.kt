package th.co.dv.p2p.customs.repository

import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import th.co.dv.p2p.customs.model.CreditNoteItemR9
import th.co.dv.p2p.customs.model.ReconcileResult
import th.co.dv.p2p.customs.model.ReconcileResultId

interface ReconcileResultRepository : PagingAndSortingRepository<ReconcileResult, ReconcileResultId>, JpaSpecificationExecutor<ReconcileResult> {
}
