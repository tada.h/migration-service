package th.co.dv.p2p.customs.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class ResponseModelWithType<T> (

        @JsonProperty("statusCode")
        val statusCode: Int? = null,

        @JsonProperty("message")
        val message: String? = null,

        @JsonProperty("data")
        val data: T? = null)