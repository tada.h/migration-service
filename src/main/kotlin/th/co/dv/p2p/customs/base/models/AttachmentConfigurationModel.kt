package th.co.dv.p2p.customs.base.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude

/**
 * Model for attachment offledger data
 *
 * @property companyTaxId The compamy tax code
 * @property documentType The type of document (Invoice/CreditNote)
 * @property attachmentType The type of attachment i.e. TaxInvoice, Receipt, DeliveryNote, Others, CreditNote
 * @property minimumNumberOfFiles The minimum number of attachment files that server allow to upload i.e. 2 it 's mean you must to upload at least 2 files
 * @property maximumNumberOfFiles The maximum number of attachment files that server allow to upload i.e. 3 it 's mean you must to upload not more than 3 files
 * @property fileType The extension of attachment that server allow to upload
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class AttachmentConfigurationModel(
        var companyTaxId: String? = null,
        var documentType: String? = null,
        var attachmentType: String? = null,
        var minimumNumberOfFiles: Int? = null,
        var maximumNumberOfFiles: Int? = null,
        var fileType: String? = null)