package th.co.dv.p2p.customs.base.models

/**
 * Data class for search posting document in Corda
 */
data class PostingSearchModel(
        val page: Int = 1,
        val pageSize: Int = 500,
        val statuses: List<String>? = null,
        val returnInvoiceItems: Boolean? = false,
        val returnGoodsReceiveds: Boolean? = false,
        val postingStatus: List<String>? = null,
        val returnDebitNoteItems: Boolean = false,
        val returnPurchaseItems: Boolean = false,
        val returnInvoice: Boolean? = false,
        val sellerStatus: Boolean = false
)