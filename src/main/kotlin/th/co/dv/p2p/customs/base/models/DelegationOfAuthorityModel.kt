package th.co.dv.p2p.customs.base.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import java.time.Instant


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class DelegationOfAuthorityModel(
        val assignee: String? = null,
        val commonName: String? = null,
        val level: Long? = null,
        val description: String? = null,
        val approvedDate: String? = null,
        val approvedBy: String? = null,
        val assignedDate: String? = null,
        val isApproved: Boolean? = null,
        val remark: String? = null
)