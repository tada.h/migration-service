package th.co.dv.p2p.customs.base.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import org.codehaus.jackson.map.annotate.JsonSerialize
import java.math.BigDecimal

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonSerialize
data class InvoiceModel(

        @JsonProperty("vendorNumber")
        var vendorNumber: String? = null,

        @JsonProperty("vendorBranchCode")
        var vendorBranchCode: String? = null,
        @JsonProperty("vendorBranchName")
        var vendorBranchName: String? = null,
        @JsonProperty("vendorName")
        var vendorName: String? = null,
        @JsonProperty("vendorTaxNumber")
        var vendorTaxNumber: String? = null,
        @JsonProperty("vendorAddress")
        var vendorAddress: String? = null,
        @JsonProperty("vendorTelephone")
        var vendorTelephone: String? = null,
        @JsonProperty("companyCode")
        var companyCode: String? = null,
        @JsonProperty("companyName")
        var companyName: String? = null,
        @JsonProperty("companyTaxNumber")
        var companyTaxNumber: String? = null,
        @JsonProperty("companyBranchCode")
        var companyBranchCode: String? = null,
        @JsonProperty("companyBranchName")
        var companyBranchName: String? = null,
        @JsonProperty("companyAddress")
        var companyAddress: String? = null,
        @JsonProperty("companyTelephone")
        var companyTelephone: String? = null,
        @JsonProperty("businessPlace")
        var businessPlace: String? = null,
        @JsonProperty("externalId")
        var externalId: String? = null,
        @JsonProperty("invoiceDate")
        var invoiceDate: String? = null,
        @JsonProperty("approvedDate")
        var approvedDate: String? = null,
        @JsonProperty("invoiceFinancing")
        var invoiceFinancing: Char? = null,
        @JsonProperty("invoiceFinancedDate")
        var invoiceFinancedDate: String? = null,
        @JsonProperty("paymentTermCode")
        var paymentTermCode: String? = null,
        @JsonProperty("paymentTermDesc")
        var paymentTermDesc: String? = null,
        @JsonProperty("paymentTermDays")
        var paymentTermDays: Long? = null,
        @JsonProperty("dueDate")
        var dueDate: String? = null,
        @JsonProperty("initialDueDate")
        var initialDueDate: String? = null,
        @JsonProperty("dueDateLastEditedReason")
        var dueDateLastEditedReason: String? = null,
        @JsonProperty("dueDateLastEditedBy")
        var dueDateLastEditedBy: String? = null,
        @JsonProperty("dueDateLastEditedDate")
        var dueDateLastEditedDate: String? = null,
        @JsonProperty("dueDateIsLocked")
        var dueDateIsLocked: Boolean? = false,
        @JsonProperty("invoiceCreatedDate")
        var invoiceCreatedDate: String? = null,
        @JsonProperty("listTotal")
        var listTotal: BigDecimal? = null,
        @JsonProperty("totalDiscount")
        var totalDiscount: BigDecimal? = null,
        @JsonProperty("totalSurcharge")
        var totalSurcharge: BigDecimal? = null,
        @JsonProperty("subTotal")
        var subTotal: BigDecimal? = null,
        @JsonProperty("vatTotal")
        var vatTotal: BigDecimal? = null,
        @JsonProperty("invoiceTotal")
        var invoiceTotal: BigDecimal? = null,
        @JsonProperty("totalPayable")
        var totalPayable: BigDecimal? = null,
        @JsonProperty("retentionAmount")
        var retentionAmount: BigDecimal? = null,
        @JsonProperty("retentionFromGR")
        var retentionFromGR: Boolean? = null,
        @JsonProperty("currency")
        var currency: String? = null,
        @JsonProperty("matchedCode")
        var matchedCode: Map<String, String> = emptyMap(),
        @JsonProperty("unmatchedCode")
        var unmatchedCode: List<String>? = null,
        @JsonProperty("unmatchedReason")
        var unmatchedReason: String? = null,
        @JsonProperty("buyer")
        var buyer: PartyModel? = null,
        @JsonProperty("seller")
        var seller: PartyModel? = null,
        @JsonProperty("accounting")
        var accounting: PartyModel? = null,
        @JsonProperty("bank")
        var bank: PartyModel? = null,
        @JsonProperty("resubmitCount")
        var resubmitCount: Int? = 0,
        @JsonProperty("baselineDate")
        var baselineDate: String? = null,
        @JsonProperty("lastMatchUpdatedDate")
        var lastMatchUpdatedDate: String? = null,
        @JsonProperty("purchaseOrder")
        var purchaseOrder: String? = null,
        @JsonProperty("purchaseOrderHeaderNumber")
        var purchaseOrderHeaderNumber: String? = null,
        @JsonProperty("goodsReceived")
        var goodsReceived: String? = null,
        @JsonProperty("receiptNumber")
        var receiptNumber: String? = null,
        @JsonProperty("paymentFee")
        var paymentFee: BigDecimal? = null,
        @JsonProperty("items")
        var invoiceItems: List<InvoiceItemModel>? = emptyList(),
        @JsonProperty("status")
        var status: String? = null,
        @JsonProperty("lifecycle")
        var lifecycle: String? = null,
        @JsonProperty("linearId")
        var linearId: String? = null,
        @JsonProperty("buyerApprovedDate")
        var buyerApprovedDate: String? = null,
        @JsonProperty("buyerApprovedRemark")
        var buyerApprovedRemark: String? = null,
        @JsonProperty("buyerApprovedUser")
        var buyerApprovedUser: String? = null,
        @JsonProperty("buyerRejectedDate")
        var buyerRejectedDate: String? = null,
        @JsonProperty("buyerRejectedRemark")
        var buyerRejectedRemark: String? = null,
        @JsonProperty("buyerRejectedUser")
        var buyerRejectedUser: String? = null,
        @JsonProperty("buyerClarifiedDate")
        var buyerClarifiedDate: String? = null,
        @JsonProperty("buyerClarifiedRemark")
        var buyerClarifiedRemark: String? = null,
        @JsonProperty("buyerClarifiedUser")
        var buyerClarifiedUser: String? = null,
        @JsonProperty("cancelledUser")
        var cancelledUser: String? = null,
        @JsonProperty("cancelledDate")
        var cancelledDate: String? = null,
        @JsonProperty("cancelledRemark")
        var cancelledRemark: String? = null,
        @JsonProperty("cancelledCode")
        var cancelledCode: String? = null,
        @JsonProperty("cancelledInvoiceNumber")
        var cancelledInvoiceNumber: String? = null,
        @JsonProperty("cancelledVendorTaxNumber")
        var cancelledVendorTaxNumber: String? = null,
        @JsonProperty("cancelledInvoiceDate")
        var cancelledInvoiceDate: String? = null,
        @JsonProperty("calculatedDate")
        var calculatedDate: String? = null,
        @JsonProperty("customisedFields")
        var customisedFields: Map<String, Any> = emptyMap(),
        @JsonProperty("customisedFieldsUpdatedDate")
        var customisedFieldsUpdatedDate: String? = null,
        @JsonProperty("currentAuthority")
        val currentAuthority: DelegationOfAuthorityModel? = null,
        @JsonProperty("nextAuthority")
        val nextAuthority: DelegationOfAuthorityModel? = null,
        @JsonProperty("previousAuthority")
        val previousAuthority: DelegationOfAuthorityModel? = null,
        @JsonProperty("delegatedAuthorities")
        val delegatedAuthorities: List<DelegationOfAuthorityModel>? = null,
        @JsonProperty("taggedCreditNotes")
        var taggedCreditNotes: List<TaggedCreditNoteModel>? = null,
        @JsonProperty("paymentDate")
        var paymentDate: String? = null,
        @JsonProperty("paymentItemLinearId")
        var paymentItemLinearId: String? = null,
        @JsonProperty("paymentIsSuccessful")
        var paymentIsSuccessful: Boolean? = null,
        @JsonProperty("paymentStatusUpdateDate")
        var paymentStatusUpdateDate: String? = null,
        @JsonProperty("fileAttachments")
        var fileAttachments: List<FileAttachmentModel>? = listOf(),
        @JsonProperty("invoicePostingIsSuccessful")
        var invoicePostingIsSuccessful: Boolean? = null,
        @JsonProperty("postingStatus")
        var postingStatus: String? = null,
        @JsonProperty("paymentReferenceNumber")
        var paymentReferenceNumber: String? = null,
        @JsonProperty("invoicePostingUpdatedDate")
        var invoicePostingUpdatedDate: String? = null,
        @JsonProperty("lastEditedBy")
        var lastEditedBy: String? = null,
        @JsonProperty("lastEditedDate")
        var lastEditedDate: String? = null,
        @JsonProperty("isOnHold")
        var isOnHold: Boolean? = null,
        @JsonProperty("lastHeldBy")
        var lastHeldBy: String? = null,
        @JsonProperty("lastHeldRemark")
        var lastHeldRemark: String? = null,
        @JsonProperty("lastHeldDate")
        var lastHeldDate: String? = null,
        @JsonProperty("lastUnheldBy")
        var lastUnheldBy: String? = null,
        @JsonProperty("lastUnheldRemark")
        var lastUnheldRemark: String? = null,
        @JsonProperty("lastUnheldDate")
        var lastUnheldDate: String? = null,
        @JsonProperty("calendarKey")
        var calendarKey: String? = null,
        @JsonProperty("referenceField1")
        var referenceField1: String? = null,
        @JsonProperty("referenceField2")
        var referenceField2: String? = null,
        @JsonProperty("referenceField3")
        var referenceField3: String? = null,
        @JsonProperty("referenceField4")
        var referenceField4: String? = null,
        @JsonProperty("referenceField5")
        var referenceField5: String? = null,
        @JsonProperty("isETaxInvoice")
        var isETaxInvoice: Boolean? = null,
        @JsonProperty("matchingStatus")
        var matchingStatus: String? = null,
        @JsonProperty("withholdingTaxCalculationPoint")
        var withholdingTaxCalculationPoint: String? = null,
        @JsonProperty("purchaseOrderModel")
        var purchaseOrderModel: PurchaseOrderModel? = null,
        @JsonProperty("removeFromPayment")
        var removeFromPayment: Boolean? = null
)