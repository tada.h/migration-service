package th.co.dv.p2p.customs.model

import java.math.BigDecimal
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "invoice_r9")
class InvoiceR9(

        @Column(name = "linear_id")
        var linearId: String? = null,

        @Column(name = "vendor_number")
        var vendorNumber: String? = null,

        @Column(name = "vendor_branch_code")
        var vendorBranchCode: String? = null,

        @Column(name = "vendor_branch_name")
        var vendorBranchName: String? = null,

        @Column(name = "vendor_name")
        var vendorName: String? = null,

        @Column(name = "vendor_name_lowercase")
        var vendorNameLowerCase: String? = null,

        @Column(name = "vendor_tax_number")
        var vendorTaxNumber: String? = null,

        @Column(name = "vendor_address", columnDefinition = "TEXT")
        var vendorAddress: String? = null,

        @Column(name = "vendor_telephone")
        var vendorTelephone: String? = null,

        @Column(name = "company_code")
        var companyCode: String? = null,

        @Column(name = "company_name")
        var companyName: String? = null,

        @Column(name = "company_branch_code")
        var companyBranchCode: String? = null,

        @Column(name = "company_branch_name")
        var companyBranchName: String? = null,

        @Column(name = "company_tax_number")
        var companyTaxNumber: String? = null,

        @Column(name = "company_address", columnDefinition = "TEXT")
        var companyAddress: String? = null,

        @Column(name = "company_telephone")
        var companyTelephone: String? = null,

        @Column(name = "business_place", columnDefinition = "TEXT")
        var businessPlace: String? = null,

        @Column(name = "external_id")
        var externalId: String? = null,

        // Hibernate function to convert to lowercase in SQL schema.
        // The BLOB stored by corda core is still in original format.
        // For 3WM/2WM purposes, we use this column to compare against a lowered cased operand
        // i.e column.externalId equals lowercase(input)
        @Column(name = "external_id_lowercase")
//        @ColumnTransformer(write = "lower(?)")
        var externalIdLowerCase: String? = externalId,

        @Column(name = "invoice_date")
        var invoiceDate: Date? = null,

        @Column(name = "invoice_created_date")
        var invoiceCreatedDate: Date? = null,

        @Column(name = "invoice_financing")
        var invoiceFinancing: Char? = null,

        @Column(name = "payment_term_code")
        var paymentTermCode: String? = null,

        @Column(name = "payment_term_days")
        var paymentTermDays: Long? = null,

        @Column(name = "payment_term_desc", columnDefinition = "TEXT")
        var paymentTermDesc: String? = null,

        @Column(name = "due_date")
        var dueDate: Date? = null,

        @Column(name = "sub_total")
        var subTotal: Long? = null,

        @Column(name = "vat_code")
        var vatCode: String? = null,

        @Column(name = "vat_rate")
        var vatRate: BigDecimal? = null,

        @Column(name = "vat_total")
        var vatTotal: Long? = null,

        @Column(name = "total")
        var total: Long? = null,

        @Column(name = "total_payable")
        var totalPayable: Long? = null,

        @Column(name = "currency", length = 3)
        var currency: String? = null,

        @Column(name = "matched_code", nullable = false)
//        @Type(type = "org.hibernate.type.ImageType")
        var matchedCode: String? = null,

        @Column(name = "unmatched_reason", columnDefinition = "TEXT")
        var unmatchedReason: String? = null,

        @Column(name = "unmatched_code")
        var unmatchedCode: String? = null,

        @Column(name = "buyer")
        var buyer: String? = null,

        @Column(name = "seller")
        var seller: String? = null,

        @Column(name = "accounting")
        var accounting: String? = null,

        @Column(name = "bank")
        var bank: String? = null,

        @Column(name = "lifecycle")
        var lifecycle: String? = null,

        @Column(name = "payment_item_linear_id")
        var paymentItemLinearId: String? = null,

        @Column(name = "payment_status")
        var paymentStatus: String? = null,

        @Column(name = "payment_status_update_date")
        var paymentStatusUpdateDate: Date? = null,

        @Column(name = "last_match_updated_date")
        var lastMatchUpdatedDate: Date? = null,

        @Column(name = "issued_date")
        val issuedDate: Date? = null,

        @Column(name = "initial_due_date")
        var initialDueDate: Date? = null,

        @Column(name = "due_date_last_edited_reason")
        var dueDateLastEditedReason: String? = null,

        @Column(name = "due_date_last_edited_user")
        var dueDateLastEditedBy: String? = null,

        @Column(name = "due_date_last_edited_date")
        var dueDateLastEditedDate: Date? = null,

        @Column(name = "due_date_is_locked")
        var dueDateIsLocked: Boolean = false,

        @Column(name = "buyer_approved_date")
        var buyerApprovedDate: Date? = null,

        @Column(name = "buyer_approved_remark", columnDefinition = "TEXT")
        var buyerApprovedRemark: String? = null,

        @Column(name = "buyer_approved_user")
        var buyerApprovedUser: String? = null,

        @Column(name = "buyer_rejected_date")
        var buyerRejectedDate: Date? = null,

        @Column(name = "buyer_rejected_remark", columnDefinition = "TEXT")
        var buyerRejectedRemark: String? = null,

        @Column(name = "buyer_rejected_user")
        var buyerRejectedUser: String? = null,

        @Column(name = "buyer_clarified_date")
        var buyerClarifiedDate: Date? = null,

        @Column(name = "buyer_clarified_remark", columnDefinition = "TEXT")
        var buyerClarifiedRemark: String? = null,

        @Column(name = "buyer_clarified_user")
        var buyerClarifiedUser: String? = null,

        @Column(name = "reject_before_doa_remark")
        var rejectBeforeDOARemark: String? = null,

        @Column(name = "reject_before_doa_date")
        var rejectBeforeDOADate: Date? = null,

        @Column(name = "reject_before_doa_by")
        var rejectBeforeDOABy: String? = null,

        @Column(name = "cancelled_user")
        var cancelledUser: String? = null,

        @Column(name = "cancelled_date")
        var cancelledDate: Date? = null,

        @Column(name = "cancelled_remark", columnDefinition = "TEXT")
        var cancelledRemark: String? = null,

        @Column(name = "resubmit_count")
        var resubmitCount: Int? = 0,

        @Column(name = "baseline_date")
        var baselineDate: Date? = null,

        @Column(name = "invoiceinvoice_financing_date")
        var invoiceFinancedDate: Date? = null,

        @Column(name = "cancelled_code")
        var cancelledCode: String? = null,

        @Column(name = "payment_date")
        var paymentDate: Date? = null,

        @Column(name = "calculated_date")
        var calculatedDate: Date? = null,

        @Column(name = "customised_fields", nullable = false)
//        @Type(type = "org.hibernate.type.ImageType")
        var customisedFields: String? = null,

        @Column(name = "customised_fields_updated_date")
        var customisedFieldsUpdatedDate: Date? = null,

        @Column(name = "invoice_posting_is_successful")
        var invoicePostingIsSuccessful: Boolean? = null,

        @Column(name = "invoice_posting_updated_date")
        var invoicePostingIsUpdatedDate: Date? = null,

        @Column(name = "custom_indicator_1")
        var customIndicator1: Boolean = false,

        @Column(name = "payment_reference_number")
        var paymentReferenceNumber: String? = null,

        @Column(name = "current_authority_assignee")
        var currentAuthorityAssignee: String? = null,

        @Column(name = "current_authority_common_name")
        var currentAuthorityCommonName: String? = null,

        @Column(name = "current_authority_level")
        var currentAuthorityLevel: Long? = null,

        @Column(name = "current_authority_description", columnDefinition = "TEXT")
        var currentAuthorityDescription: String? = null,

        @Column(name = "current_authority_assigned_date")
        var currentAuthorityAssignedDate: Date? = null,

        @Column(name = "next_authority_assignee")
        var nextAuthorityAssignee: String? = null,

        @Column(name = "next_authority_common_name")
        var nextAuthorityCommonName: String? = null,

        @Column(name = "next_authority_level")
        var nextAuthorityLevel: Long? = null,

        @Column(name = "next_authority_description", columnDefinition = "TEXT")
        var nextAuthorityDescription: String? = null,

        @Column(name = "previous_authority_assignee")
        var previousAuthorityAssignee: String? = null,

        @Column(name = "previous_authority_common_name")
        var previousAuthorityCommonName: String? = null,

        @Column(name = "previous_authority_level")
        var previousAuthorityLevel: Long? = null,

        @Column(name = "previous_authority_description", columnDefinition = "TEXT")
        var previousAuthorityDescription: String? = null,

        @Column(name = "previous_approved_date")
        var previousApprovedDate: Date? = null,

        @Column(name = "previous_authority_is_approved")
        var previousAuthorityIsApproved: Boolean? = null,

        @Column(name = "previous_authority_remark", columnDefinition = "TEXT")
        var previousAuthorityRemark: String? = null,

        @Column(name = "previous_authority_assigned_date")
        var previousAuthorityAssignedDate: Date? = null,

        @Column(name = "approved_date")
        var approvedDate: Date? = null,

        @Column(name = "list_total")
        var listTotal: Long? = null,

        @Column(name = "total_discount")
        var totalDiscount: Long? = null,

        @Column(name = "total_surcharge")
        var totalSurcharge: Long? = null,

        @Column(name = "is_e_tax_invoice")
        var isETaxInvoice: Boolean? = null,

        @Column(name = "receipt_number")
        var receiptNumber: String? = null,

        @Column(name = "payment_fee")
        var paymentFee: Long? = null,

        @Column(name = "reference_field_1")
        var referenceField1: String? = null,

        @Column(name = "reference_field_2")
        var referenceField2: String? = null,

        @Column(name = "reference_field_3")
        var referenceField3: String? = null,

        @Column(name = "reference_field_4")
        var referenceField4: String? = null,

        @Column(name = "reference_field_5")
        var referenceField5: String? = null,

        @Column(name = "last_edited_by")
        var lastEditedBy: String? = null,

        @Column(name = "last_edited_date")
        var lastEditedDate: Date? = null,

        @Column(name = "last_held_by")
        var lastHeldBy: String? = null,

        @Column(name = "last_held_remark", columnDefinition = "TEXT")
        var lastHeldRemark: String? = null,

        @Column(name = "last_held_date")
        var lastHeldDate: Date? = null,

        @Column(name = "is_on_hold")
        var isOnHold: Boolean? = null,

        @Column(name = "last_unheld_by")
        var lastUnheldBy: String? = null,

        @Column(name = "last_unheld_remark", columnDefinition = "TEXT")
        var lastUnheldRemark: String? = null,

        @Column(name = "last_unheld_date")
        var lastUnheldDate: Date? = null,

        @Column(name = "withholding_tax_calculation_point")
        var withholdingTaxCalculationPoint: String? = null,

        @Column(name = "calendar_key")
        var calendarKey: String? = null,

        @Column(name = "posting_status")
        var postingStatus: String? = null,

        @Column(name = "retention_amount")
        val retentionAmount: Long? = null,

        @Column(name = "retention_from_gr")
        val retentionFromGR: Boolean? = null,

        @Column(name = "payment_term_months")
        val paymentTermMonths: Int? = null,

        @Column(name = "vat_trigger_point")
        val vatTriggerPoint: String? = null

//        @OneToMany(fetch = FetchType.LAZY, cascade = arrayOf(CascadeType.PERSIST))
//        @JoinColumns(
//                JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id"),
//                JoinColumn(name = "output_index", referencedColumnName = "output_index"))
//        @OrderColumn
//        var delegatedAuthorities: List<DelegationOfAuthorityEntity>? = null,
//
//        @OneToMany(fetch = FetchType.LAZY, cascade = arrayOf(CascadeType.PERSIST))
//        @JoinColumns(
//                JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id"),
//                JoinColumn(name = "output_index", referencedColumnName = "output_index"))
//        @OrderColumn
//        var invoiceTaggedCreditNote: Set<InvoiceTaggedCreditNoteEntity>? = null,
//
//        @OneToMany(fetch = FetchType.LAZY, cascade = arrayOf(CascadeType.PERSIST))
//        @JoinColumns(
//                JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id"),
//                JoinColumn(name = "output_index", referencedColumnName = "output_index"))
//        @OrderColumn
//        var invoiceAttachments: List<InvoiceAttachmentEntity> = listOf()

)
