package th.co.dv.p2p.customs.base.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class SendInvoiceWaitingForDoaApprovalEmailModel (

        val currentAuthority: DelegationOfAuthorityModel? = null,
        val companyName : String? = null,
        val vendorName : String? = null,
        val documentNo : String? = null,
        val documentType: String? = null,
        val paymentDueDate: String? = null,
        val invoiceFinancing : Char? = null,
        val unmatchedCode: List<String>? = null,
        val matchedCode: Map<String, String> = emptyMap()






)