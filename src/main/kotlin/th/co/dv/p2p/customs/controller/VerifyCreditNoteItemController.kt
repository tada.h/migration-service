package th.co.dv.p2p.customs.controller

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import th.co.dv.p2p.customs.model.ReconcileResult
import th.co.dv.p2p.customs.model.ReconcileResultId
import th.co.dv.p2p.customs.base.utilities.Conditions.using
import th.co.dv.p2p.customs.base.utilities.trySilently
import th.co.dv.p2p.customs.repository.CreditNoteItemR6Repository as PreviousStateRepository
import th.co.dv.p2p.customs.repository.CreditNoteItemR9Repository as TargetStateRepository
import th.co.dv.p2p.customs.utilities.release9.CreditNoteItemR9ModelUtils as TargetUtils
import th.co.dv.p2p.customs.repository.ReconcileResultRepository

import th.co.dv.p2p.customs.service.corda.CordaService

@RestController
@RequestMapping("/api/verifycreditnoteItem")
class VerifyCreditNoteItemController {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }

    private final val STATE_NAME = "CREDIT_NOTE_ITEM"
    private final val TARGET_RELEASE = "RELEASE-009"

    @Autowired
    lateinit var cordaService: CordaService

    @Autowired
    lateinit var previousStateRepository: PreviousStateRepository

    @Autowired
    lateinit var targetStateRepository: TargetStateRepository

    @Autowired
    lateinit var reconcileResultRepository: ReconcileResultRepository


    @PostMapping("")
    fun verifyState(): Pair<HttpStatus,String> {

        val reconcileResult = mutableListOf<ReconcileResult>()
        val previousStateList = previousStateRepository.findAll()
        val targetStateList = targetStateRepository.findByExternalIdIn(previousStateList.map { it.externalId!! })
        val migrateUtil = TargetUtils()

        previousStateList.forEach {  previousState ->
            val targetState = targetStateList.find { it.externalId == previousState.externalId && it.creditNoteLinearId == previousState.creditNoteLinearId}

            trySilently {
                val STATE_KEY = previousState.creditNoteLinearId + " : " + previousState.externalId
                if(targetState == null){
                    reconcileResult.add(ReconcileResult(
                            reconcileResultID = ReconcileResultId(
                                    state = STATE_NAME,
                                    stateKey = STATE_KEY,
                                    type = "MISSING"),
                            stateRemark = "Cannot find $STATE_NAME $TARGET_RELEASE"
                    ))
                }
                "Cannot find $STATE_NAME $TARGET_RELEASE $STATE_KEY" using (targetState != null)

                val result = migrateUtil.compareState(previousState, targetState!!)
                result.forEach {
                    val remark = it.first
                    val fieldList = it.second
                    var mismatchFields = ""

                    fieldList.forEach { fieldName ->
                        mismatchFields = "$mismatchFields,$fieldName"
                    }

                    if (fieldList.size > 0) {
                        reconcileResult.add(ReconcileResult(
                                reconcileResultID = ReconcileResultId(
                                        state = STATE_NAME,
                                        stateKey = STATE_KEY,
                                        type = remark),
                                stateRemark = mismatchFields.substring( 1 , mismatchFields.length )
                        ))
                    }
                }
            }
        }

        logger.info("verifyState.ReconcileResult : $reconcileResult")
        reconcileResultRepository.saveAll(reconcileResult)

        return HttpStatus.OK to "Success"
    }





}
