package th.co.dv.p2p.customs.repository

import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import th.co.dv.p2p.customs.model.CreditNoteItemR6

@Repository
interface CreditNoteItemR6Repository : PagingAndSortingRepository<CreditNoteItemR6, Long>, JpaSpecificationExecutor<CreditNoteItemR6> {
}
