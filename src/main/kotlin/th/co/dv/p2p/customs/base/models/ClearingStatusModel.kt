package th.co.dv.p2p.customs.base.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class ClearingStatusModel(
        val buyerCompanyCode: String? = null,
        val fiscalYearOfPaymentDocumentNo: String? = null,
        val externalId: String? = null,
        val clearingDocumentNo: String? = null,
        val fiscalYearOfClearingDocumentNo: String? = null,
        val status: String? = null,
        val lifecycle: String? = null,
        val buyer: PartyModel? = null,
        val seller: PartyModel? = null,
        val linearId: String? = null)