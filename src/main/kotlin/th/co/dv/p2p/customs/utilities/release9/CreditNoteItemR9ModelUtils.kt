package th.co.dv.p2p.customs.utilities.release9

import th.co.dv.p2p.customs.base.MigrateData
import th.co.dv.p2p.customs.base.utilities.trySilently
import th.co.dv.p2p.customs.model.CreditNoteItemR6 as PreviousStateClass
import th.co.dv.p2p.customs.model.CreditNoteItemR9 as TargetStateClass
import java.math.BigDecimal
import kotlin.reflect.full.memberProperties

data class CreditNoteItemR9ModelUtils(
        val version: String? = "RELEASE9"
) : MigrateData{
    override val newFields: List<String> = listOf()
    override val equalFields: List<String> = listOf("linearId", "buyer", "seller")
    override val zero2DigitsFields: List<String> = listOf()
    override val zero3DigitsFields: List<String> = listOf("quantity")

    fun compareState(previousState : PreviousStateClass, targetState : TargetStateClass): MutableList<Pair<String,MutableList<String>>>{
        val resultNotEqualFields = processCompare(previousState, targetState, equalFields, "EQUAL")
        val resultNotEqualNewFields = processCompare(previousState, targetState, newFields, "NEWFIELD")
        val resultNotZero2DigitsFields = processCompare(previousState, targetState, zero2DigitsFields, "ZERO2DIGITS")
        val resultNotZero3DigitsFields = processCompare(previousState, targetState, zero3DigitsFields, "ZERO3DIGITS")
        val summaryResult = mutableListOf<Pair<String,MutableList<String>>>()

        summaryResult.add("NOT_EQUAL" to resultNotEqualFields)
        summaryResult.add("NOT_EQUAL_NEW_FIELD" to resultNotEqualNewFields)
        summaryResult.add("NOT_EQUAL_ZERO_2_DIGITS" to resultNotZero2DigitsFields)
        summaryResult.add("NOT_EQUAL_ZERO_3_DIGITS" to resultNotZero3DigitsFields)
        return summaryResult
    }

    fun processCompare(previousState : PreviousStateClass, targetState : TargetStateClass, listField: List<String>, type: String): MutableList<String>{
        val result = mutableListOf<String>()
        trySilently {
            listField.forEach { fieldName ->
                var propertyOfRelease6 = PreviousStateClass::class.memberProperties.find { it -> it.name == fieldName }!!
                var propertyOfRelease9 = TargetStateClass::class.memberProperties.find { it -> it.name == fieldName }!!
                val fieldFromPrevious = propertyOfRelease6.get(previousState)
                val fieldFromTarget = propertyOfRelease9.get(targetState)
                when(type){
                    "EQUAL" -> when( fieldFromPrevious != fieldFromTarget ) { true -> result.add(fieldName) }
                    "NEWFIELD" -> when( fieldFromTarget == null ) { true -> result.add(fieldName) }
                    "ZERO2DIGITS" -> when( fieldFromTarget != BigDecimal.ZERO.setScale(2) ) { true -> result.add(fieldName) }
                    "ZERO3DIGITS" -> when( fieldFromTarget != BigDecimal.ZERO.setScale(3) ) { true -> result.add(fieldName) }
                }
            }
        }
        return result
    }
}