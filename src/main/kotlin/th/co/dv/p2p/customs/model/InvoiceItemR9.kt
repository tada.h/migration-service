package th.co.dv.p2p.customs.model

import java.math.BigDecimal
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "goods_received_item_r9")
class InvoiceItemR9(


        @Column(name = "buyer")
        var buyer: String? = null,

        @Column(name = "seller")
        var seller: String? = null,

        @Column(name = "accounting")
        var accounting: String? = null,

        @Column(name = "bank")
        var bank: String? = null,

        @Column(name = "external_id")
        var externalId: String? = null,

        @Column(name = "linear_id")
        var linearId: String? = null,

        @Column(name = "invoice_linear_id")
        var invoiceLinearId: String? = null,

        @Column(name = "purchase_order_external_id")
        var purchaseOrderExternalId: String? = null,

        @Column(name = "purchase_item_linear_id")
        var purchaseItemLinearId: String? = null,

        @Column(name = "purchase_item_external_id")
        var purchaseItemExternalId: String? = null,

        @Column(name = "material_description", columnDefinition = "TEXT")
        var materialDescription: String? = null,

        @Column(name = "material_group")
        var materialGroup: String? = null,

        @Column(name = "quantity", precision = 19, scale = 3)
        var quantity: BigDecimal? = null,

        @Column(name = "unit_price")
        var unitPrice: Long? = null,

        @Column(name = "estimated_unit_price")
        var estimatedUnitPrice: String? = null,

        @Column(name = "currency", length = 3)
        var currency: String? = null,

        @Column(name = "vat_code")
        var vatCode: String? = null,

        @Column(name = "vat_rate")
        var vatRate: BigDecimal? = null,

        @Column(name = "sub_total")
        var subTotal: Long? = null,

        @Column(name = "withholding_tax_rate")
        var withholdingTaxRate: BigDecimal? = null,

        @Column(name = "withholding_tax_form_type")
        var withholdingTaxFormType: String? = null,

        @Column(name = "withholding_tax_pay_type")
        var withholdingTaxPayType: String? = null,

        @Column(name = "withholding_tax_remark")
        var withholdingTaxRemark: String? = null,

        @Column(name = "withholding_tax_income_type")
        var withholdingTaxIncomeType: String? = null,

        @Column(name = "withholding_tax_income_description", columnDefinition = "TEXT")
        var withholdingTaxIncomeDescription: String? = null,

        @Column(name = "withholding_tax_code")
        var withholdingTaxCode: String? = null,

        @Column(name = "matched_code", nullable = false)
//        @Type(type = "org.hibernate.type.ImageType")
        var matchedCode: String? = null,

        @Column(name = "unmatched_code")
        var unmatchedCode: String? = null,

        @Column(name = "unmatched_reason", columnDefinition = "TEXT")
        var unmatchedReason: String? = null,

        @Column(name = "last_match_updated_date")
        var lastMatchUpdatedDate: Date? = null,

        @Column(name = "lifecycle")
        var lifecycle: String? = null,

        @Column(name = "issued_date")
        val issuedDate: Date? = null,

        @Column(name = "list_unit_price")
        var listUnitPrice: Long? = null,

        @Column(name = "unit_price_discount")
        var unitPriceDiscount: Long? = null,

        @Column(name = "unit_price_surcharge")
        var unitPriceSurcharge: Long? = null,

        @Column(name = "vat_total")
        var vatTotal: Long? = null,

        @Column(name = "total")
        var total: Long? = null,

        @Column(name = "credit_note_quantity", precision = 19, scale = 3)
        var creditNoteQuantity: BigDecimal? = null,

        @Column(name = "total_discount")
        var totalDiscount: Long? = null,

        @Column(name = "total_surcharge")
        var totalSurcharge: Long? = null,

        @Column(name = "credit_note_adjusted_subtotal")
        var creditNoteAdjustedSubtotal: Long? = null,

        @Column(name = "debit_note_adjusted_subtotal")
        var debitNoteAdjustedSubtotal: Long? = null,

        @Column(name = "site")
        var site: String? = null,

        @Column(name = "site_description", columnDefinition = "TEXT")
        var siteDescription: String? = null,

        @Column(name = "section")
        var section: String? = null,

        @Column(name = "section_description", columnDefinition = "TEXT")
        var sectionDescription: String? = null,

        @Column(name = "unit_description")
        var unitDescription: String? = null,

        @Column(name = "retention_amount")
        val retentionAmount: Long? = null,

        @Column(name = "reference_field_1")
        var referenceField1: String? = null,

        @Column(name = "reference_field_2")
        var referenceField2: String? = null,

        @Column(name = "reference_field_3")
        var referenceField3: String? = null,

        @Column(name = "reference_field_4")
        var referenceField4: String? = null,

        @Column(name = "reference_field_5")
        var referenceField5: String? = null,

        @Column(name = "customised_fields", nullable = false)
//        @Type(type = "org.hibernate.type.ImageType")
        var customisedFields: String? = null,

        @Column(name = "customised_fields_updated_date")
        var customisedFieldsUpdatedDate: Date? = null

)
