package th.co.dv.p2p.customs.base.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import java.math.BigDecimal

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class TaggedDocumentModel (
        val linearId: String? = null,
        val itemLinearId: List<String>? = null,
        val externalId: String? = null,
        val knockedAmount: BigDecimal? = null
)