package th.co.dv.p2p.customs.utilities

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.Serializable

/**
 * The UserAuthorization stores information about current logged in user
 *
 * @property username username of the logged in user
 * @property companiesCode companies code of the logged in user
 * @property authorities user's authorities (What can this user do)
 * @property userGroups groups of the logged in user
 */
data class UserAuthorization(
        val username: String,
        val companiesCode: List<String> = emptyList(),
        val authorities: List<String> = emptyList(),
        val userGroups: List<UserGroup> = emptyList()
): Serializable {
    companion object {
        private val logger: Logger = LoggerFactory.getLogger(UserAuthorization::class.java)
        const val ROLE_INTERFACE = "ROLE_INTERFACE"

        /**
         * To check this user need to add filter by companyCode during query data or not
         * */
        fun needFilterAuthorization(userAuthorization: UserAuthorization): Boolean {
            logger.info("Username: ${userAuthorization.username}, Authorities: ${userAuthorization.authorities}, Companies: ${userAuthorization.companiesCode}")
            return !userAuthorization.authorities.contains(ROLE_INTERFACE)
        }

        /**
         * To check this user can query data or not
         * */
        fun hasAuthorization(userAuthorization: UserAuthorization): Boolean {
            return userAuthorization.authorities.contains(ROLE_INTERFACE)
                    || userAuthorization.companiesCode.isNotEmpty()
                    // || (userAuthorization.companiesCode.isNotEmpty()
                    // && AuthorizationUtils.hasAuthorization<T>(userAuthorization))
        }
    }

    override fun toString(): String {
        return username
    }
}

/**
 * The UserGroup stores information about group of user and what is the criteria for each states
 *
 * @property name group name
 * @property states map to store criteria for each state
 */
data class UserGroup(
    val name: String,
    val states: Map<String, List<Condition>>
)

/**
 * The Condition stores information about field, operator and value to restrict query to state
 *
 * @property field field name in state
 * @property operator operater to use in query
 * @property value value for this field
 */
data class Condition(
    val field: String,
    val operator: String,
    val value: String
)
