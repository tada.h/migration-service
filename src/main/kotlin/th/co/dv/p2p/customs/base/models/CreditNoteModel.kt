package th.co.dv.p2p.customs.base.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal

/**
 * Data model class that is passed in from the Front End that will be translated into Corda State
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class CreditNoteModel(

        @JsonProperty("linearId")
        var linearId: String? = null,
        @JsonProperty("buyer")
        var buyer: PartyModel? = null,
        @JsonProperty("seller")
        var seller: PartyModel? = null,
        @JsonProperty("accounting")
        var accounting: PartyModel? = null,
        @JsonProperty("bank")
        var bank: PartyModel? = null,
        @JsonProperty("externalId")
        var externalId: String? = null,
        @JsonProperty("invoiceExternalId")
        var invoiceExternalId: String? = null,
        @JsonProperty("invoiceLinearId")
        var invoiceLinearId: String? = null,
        @JsonProperty("vendorTaxNumber")
        var vendorTaxNumber: String? = null,
        @JsonProperty("vendorAddress")
        var vendorAddress: String? = null,
        @JsonProperty("vendorNumber")
        var vendorNumber: String? = null,
        @JsonProperty("vendorBranchCode")
        var vendorBranchCode: String? = null,
        @JsonProperty("vendorName")
        var vendorName: String? = null,
        @JsonProperty("companyTaxNumber")
        var companyTaxNumber: String? = null,
        @JsonProperty("companyAddress")
        var companyAddress: String? = null,
        @JsonProperty("companyBranchCode")
        var companyBranchCode: String? = null,
        @JsonProperty("businessPlace")
        var businessPlace: String? = null,
        @JsonProperty("companyCode")
        var companyCode: String? = null,
        @JsonProperty("companyBranchName")
        var companyBranchName: String? = null,
        @JsonProperty("companyName")
        var companyName: String? = null,
        @JsonProperty("subTotal")
        var subTotal: BigDecimal? = null,
        @JsonProperty("vatTotal")
        var vatTotal: BigDecimal? = null,
        @JsonProperty("total")
        var total: BigDecimal? = null,
        val totalReceivable: BigDecimal? = null,
        @JsonProperty("currency")
        var currency: String? = null,
        @JsonProperty("reason")
        var reason: String? = null,
        @JsonProperty("creditNoteDate")
        var creditNoteDate: String? = null,
        @JsonProperty("documentEntryDate")
        var documentEntryDate: String? = null,
        @JsonProperty("documentEntryMethod")
        var documentEntryMethod: String? = null,
        @JsonProperty("issuedDate")
        var issuedDate: String? = null,
        @JsonProperty("fileAttachments")
        var fileAttachments: List<FileAttachmentModel>? = listOf(),
        @JsonProperty("lifecycle")
        var lifecycle: String? = null,
        @JsonProperty("status")
        var status: String? = null,
        @JsonProperty("customisedFields")
        var customisedFields: Map<String, Any> = emptyMap(),
        @JsonProperty("customisedFieldsUpdatedDate")
        var customisedFieldsUpdatedDate: String? = null,
        @JsonProperty("adjustmentType")
        var adjustmentType: String? = null,
        @JsonProperty("creditNoteItems")
        var creditNoteItems: List<CreditNoteItemModel> = emptyList(),
        @JsonProperty("adjustmentMap")
        var adjustmentMap: Map<String, Any> = mapOf(
                "PRICE" to "Price Adjustment",
                "QUANTITY" to "Goods Return"),
        @JsonProperty("restrictedMap")
        var restrictedMap: Map<String, Any> = mapOf(
                "ISSUED" to "CN was created",
                "MISSING" to "Request CN Resubmission by BU from 2WM screen",
                "PENDING" to "Missing Return GR / Pending BU Approval",
                "MATCHED" to "Waiting for Settlement",
                "SETTLED" to "Got payment result from bank as successful"
        ),
        @JsonProperty("disclosedMap")
        var disclosedMap: Map<String, Any> = mapOf(
                "ISSUED" to "Submitted",
                "MISSING" to "Request to Resubmit",
                "PENDING" to "Verifying",
                "MATCHED" to "Waiting for Settlement",
                "SETTLED" to "Settled"
        ),
        @JsonProperty("creditPostingIsSuccessful")
        var creditPostingIsSuccessful: Boolean = false,
        @JsonProperty("postingStatus")
        var postingStatus: String? = null,
        @JsonProperty("buyerApprovedDate")
        var buyerApprovedDate: String? = null,
        @JsonProperty("lastMatchUpdatedDate")
        var lastMatchUpdatedDate: String? = null,
        @JsonProperty("withholdingTaxCalculationPoint")
        var withholdingTaxCalculationPoint: String? = null,
        @JsonProperty("requestExternalId")
        var requestExternalId: String? = null,
        @JsonProperty("requestLinearId")
        var requestLinearId: String? = null
)