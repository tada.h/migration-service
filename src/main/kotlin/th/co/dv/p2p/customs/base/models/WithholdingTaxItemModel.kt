package th.co.dv.p2p.customs.base.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import java.math.BigDecimal


/**
 *
 * The model use for mapping the WithholdingItemTax data to json object and return in the response (API)
 * This model is for release 6 onward
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class WithholdingTaxItemModel(
        val whtAmount: BigDecimal? = null,
        val whtIncomeType: String? = null,
        val whtIncomeTypeDescription: String? = null,
        val whtRate: Long? = null,
        val whtCode: String? = null,
        val incomeTypeAmount: BigDecimal? = null
)