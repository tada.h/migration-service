package th.co.dv.p2p.customs.base.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class FileAttachmentModel(

        @JsonProperty("attachmentId")
        var attachmentId: Long? = null,
        @JsonProperty("linearId")
        var linearId: String? = null,
        @JsonProperty("attachmentHash")
        var attachmentHash: String? = null,
        @JsonProperty("attachmentName")
        var attachmentName: String? = null,
        @JsonProperty("attachmentType")
        var attachmentType: String? = null,
        @JsonProperty("uploadDate")
        var uploadDate: String? = null,
        @JsonProperty("uploadBy")
        var uploadBy: String? = null
)