package th.co.dv.p2p.customs.controller

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import th.co.dv.p2p.corda.base.models.PageModel
import th.co.dv.p2p.corda.base.models.PartyModel
import th.co.dv.p2p.customs.service.corda.CordaService
import th.co.dv.p2p.customs.models.ResponseModel

@RestController
@RequestMapping("/purchaseitem")
class PurchaseItemController {
    companion object {
        private val logger: Logger = LoggerFactory.getLogger(PurchaseItemController::class.java)
    }

    @Autowired
    lateinit var cordaService: CordaService

    @GetMapping()
    fun getPurhcaseItem(): PageModel<Any> {
        val purchaseOrder = cordaService.queryPurchaseOrder()
        return purchaseOrder
    }
}
