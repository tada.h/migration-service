package th.co.dv.p2p.customs.model

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "credit_note_r9")
class CreditNoteR9(

        @Column(name = "linear_id")
        var linearId: String? = null,

        @Column(name = "buyer")
        var buyer: String? = null,

        @Column(name = "seller")
        var seller: String? = null,

        @Column(name = "accounting")
        var accounting: String? = null,

        @Column(name = "bank")
        var bank: String? = null,

        @Column(name = "external_id")
        var externalId: String? = null,

        // Hibernate function to convert to lowercase in SQL schema.
        // The BLOB stored by corda core is still in original format.
        @Column(name = "external_id_lowercase")
//        @ColumnTransformer(write = "lower(?)")
        var externalIdLowerCase: String? = externalId,

        @Column(name = "invoice_linear_id")
        var invoiceLinearId: String? = null,

        @Column(name = "invoice_external_id")
        var invoiceExternalId: String? = null,

        // Hibernate function to convert to lowercase in SQL schema.
        // The BLOB stored by corda core is still in original format.
        // For 3WM/2WM purposes, we use this column to compare against a lowered cased operand
        // i.e column.externalId equals lowercase(input)
        @Column(name = "invoice_external_id_lowercase")
//        @ColumnTransformer(write = "lower(?)")
        var invoiceExternalIdLowerCase: String? = invoiceExternalId,

        @Column(name = "vendor_tax_number")
        var vendorTaxNumber: String? = null,

        @Column(name = "vendor_address", columnDefinition = "TEXT")
        var vendorAddress: String? = null,

        @Column(name = "vendor_number")
        var vendorNumber: String? = null,

        @Column(name = "vendor_name_lowercase")
        var vendorNameLowerCase: String? = null,

        @Column(name = "vendor_branch_code")
        var vendorBranchCode: String? = null,

        @Column(name = "vendor_branch_name")
        var vendorBranchName: String? = null,

        @Column(name = "vendor_name")
        var vendorName: String? = null,

        @Column(name = "vendor_telephone")
        var vendorTelephone: String? = null,

        @Column(name = "company_tax_number")
        var companyTaxNumber: String? = null,

        @Column(name = "company_address", columnDefinition = "TEXT")
        var companyAddress: String? = null,

        @Column(name = "company_code")
        var companyCode: String? = null,

        @Column(name = "company_branch_code")
        var companyBranchCode: String? = null,

        @Column(name = "company_branch_name")
        var companyBranchName: String? = null,

        @Column(name = "company_name")
        var companyName: String? = null,

        @Column(name = "company_telephone")
        var companyTelephone: String? = null,

        @Column(name = "business_place")
        var businessPlace: String? = null,

        @Column(name = "credit_note_date")
        var creditNoteDate: Date? = null,

        @Column(name = "sub_total")
        var subTotal: Long? = null,

        @Column(name = "vat_total")
        var vatTotal: Long? = null,

        @Column(name = "total")
        var total: Long? = null,

        @Column(name = "total_receivable")
        var totalReceivable: Long? = null,

        @Column(name = "currency", length = 3)
        var currency: String? = null,

        @Column(name = "unmatched_reason", columnDefinition = "TEXT")
        var unmatchedReason: String? = null,

        @Column(name = "unmatched_code")
        var unmatchedCode: String? = null,

        @Column(name = "last_match_updated_date")
        var lastMatchUpdatedDate: Date? = null,

        @Column(name = "reason", columnDefinition = "TEXT")
        var reason: String? = null,

        @Column(name = "document_entry_date")
        var documentEntryDate: Date? = null,

        @Column(name = "document_entry_method")
        var documentEntryMethod: String? = null,

        @Column(name = "issued_date")
        var issuedDate: Date? = null,

        @Column(name = "payment_item_linear_id")
        var paymentItemLinearId: String? = null,

//        @OneToMany(fetch = FetchType.LAZY, cascade = arrayOf(CascadeType.PERSIST))
//        @JoinColumns(
//                JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id"),
//                JoinColumn(name = "output_index", referencedColumnName = "output_index"))
//        @OrderColumn
//        var taggedInvoices: Set<TaggedInvoiceEntity>? = null,
//
//        @OneToMany(fetch = FetchType.LAZY, cascade = arrayOf(CascadeType.PERSIST))
//        @JoinColumns(
//                JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id"),
//                JoinColumn(name = "output_index", referencedColumnName = "output_index"))
//        @OrderColumn
//        var taggedDebitNotes: Set<TaggedDebitNoteEntity>? = null,

        @Column(name = "sibling_payment_items", columnDefinition = "TEXT")
        var siblingPaymentItems: String? = null,

//        @OneToMany(fetch = FetchType.LAZY, cascade = arrayOf(CascadeType.PERSIST))
//        @JoinColumns(
//                JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id"),
//                JoinColumn(name = "output_index", referencedColumnName = "output_index"))
//        @OrderColumn
//        var attachments: List<AttachmentEntity> = listOf(),

        @Column(name = "lifecycle")
        var lifecycle: String? = null,

        @Column(name = "lifecycle_stage")
        var lifecycleStage: Long? = null,

        @Column(name = "credit_posting_is_successful")
        var creditPostingIsSuccessful: Boolean? = null,

        @Column(name = "credit_posting_updated_date")
        var creditPostingUpdatedDate: Date? = null,

        @Column(name = "customised_fields", nullable = false)
//        @Type(type = "org.hibernate.type.ImageType")
        var customisedFields: String? = null,

        @Column(name = "customised_fields_updated_date")
        var customisedFieldsUpdatedDate: Date? = null,

        @Column(name = "adjustment_type")
        var adjustmentType: String? = null,

        @Column(name = "buyer_approved_date")
        var buyerApprovedDate: Date? = null,

        @Column(name = "buyer_approved_remark", columnDefinition = "TEXT")
        var buyerApprovedRemark: String? = null,

        @Column(name = "buyer_approved_user")
        var buyerApprovedUser: String? = null,

        @Column(name = "buyer_rejected_date")
        var buyerRejectedDate: Date? = null,

        @Column(name = "buyer_rejected_remark", columnDefinition = "TEXT")
        var buyerRejectedRemark: String? = null,

        @Column(name = "buyer_rejected_user")
        var buyerRejectedUser: String? = null,

        @Column(name = "cancelled_date")
        var cancelledDate: Date? = null,

        @Column(name = "cancelled_remark", columnDefinition = "TEXT")
        var cancelledRemark: String? = null,

        @Column(name = "cancelled_user")
        var cancelledUser: String? = null,

        @Column(name = "resubmit_count")
        var resubmitCount: Int = 0,

        @Column(name = "reference_field_1")
        var referenceField1: String? = null,

        @Column(name = "reference_field_2")
        var referenceField2: String? = null,

        @Column(name = "reference_field_3")
        var referenceField3: String? = null,

        @Column(name = "reference_field_4")
        var referenceField4: String? = null,

        @Column(name = "reference_field_5")
        var referenceField5: String? = null,

        @Column(name = "last_edited_by")
        var lastEditedBy: String? = null,

        @Column(name = "last_edited_date")
        var lastEditedDate: Date? = null,

        @Column(name = "is_e_tax_credit_note")
        var isETaxCreditNote: Boolean? = null,

        @Column(name = "posting_status")
        var postingStatus: String? = null,

        @Column(name = "request_linear_id")
        var requestLinearId: String? = null,

        @Column(name = "request_external_id")
        var requestExternalId: String? = null,

        @Column(name = "reject_before_doa_remark")
        var rejectBeforeDOARemark: String? = null,

        @Column(name = "reject_before_doa_date")
        var rejectBeforeDOADate: Date? = null,

        @Column(name = "reject_before_doa_by")
        var rejectBeforeDOABy: String? = null,

        @Column(name = "payment_reference_number")
        var paymentReferenceNumber: String? = null,

        @Column(name = "payment_date")
        var paymentDate: Date? = null

)
