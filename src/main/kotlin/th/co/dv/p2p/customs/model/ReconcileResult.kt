package th.co.dv.p2p.customs.model

import com.sun.xml.internal.ws.developer.Serialization
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "reconcile_result")
data class ReconcileResult(

        @EmbeddedId
        var reconcileResultID: ReconcileResultId,

        @Column(name = "state_remark")
        var stateRemark: String? = null

) {
    override fun toString(): String {
        return "ReconcileResult(reconcileResultID=${reconcileResultID.state} ${reconcileResultID.stateKey}, stateRemark=$stateRemark)"
    }
}

@Embeddable
class ReconcileResultId(

         @Column(name = "state")
         var state : String,

         @Column(name = "state_key")
         var stateKey : String,

         @Column(name = "type")
         var type : String

): Serializable