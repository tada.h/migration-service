/* Copyright 2004, 2005, 2006 Acegi Technology Pty Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package th.co.dv.p2p.corda.base.utility.search.context;

import org.springframework.util.Assert;


/**
 * A <code>ThreadLocal</code>-based implementation of
 * {@link th.co.dv.p2p.corda.base.utility.search.context.SearchContextHolderStrategy}
 * .
 *
 * @author Ben Alex
 * @version $Id: ThreadLocalSearchContextHolderStrategy.java,v 1.1 2017/11/08 03:15:40 wittawai Exp $
 * @see ThreadLocal
 */
public class ThreadLocalSearchContextHolderStrategy implements SearchContextHolderStrategy
{
	// ~ Static fields/initializers
	// =====================================================================================

	private static ThreadLocal<SearchContext> contextHolder = new ThreadLocal<SearchContext>();

	// ~ Methods
	// ========================================================================================================

	public void clearContext()
	{
		contextHolder.set(null);
	}

	public SearchContext getContext()
	{
		if (contextHolder.get() == null)
		{
			contextHolder.set(new SearchContextImpl());
		}

		return (SearchContext) contextHolder.get();
	}

	public void setContext(SearchContext context)
	{
		Assert.notNull(context, "Only non-null SearchContext instances are permitted");
		contextHolder.set(context);
	}
}
