package th.co.dv.p2p.corda.base.utility.search;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import th.co.dv.p2p.corda.base.utility.DateUtility;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <pre><b>
 *	Version 	Date		Time		Developer	Remark</b>
 *	001		Sep 30, 2009	2:22:27 PM	chatchch
 * </pre>
 */
public class SearchCriterias implements SearchCondition
{
	@SuppressWarnings("unused")
	private static Log logger = LogFactory.getLog(SearchCriterias.class);

	private List<SearchCondition> criterias = new ArrayList<SearchCondition>();

	private SearchCriteria criteria = new SearchCriteria();

	private Class<?> rootObject;

	private List<OrderBy> sort;

	private int first = 0;

	private int pageSize = 10;

	private boolean paging = true;

	private boolean asc = true;

	private boolean and = true;

	private boolean distinct = false;

	private List<String> select = null;

	public SearchCriterias(Class<?> rootObject)
	{
		this.rootObject = rootObject;
	}

	public int getCriteriaSize()
	{
		return criterias.size();
	}

	/**
	 * @return the criterias
	 */
	public List<SearchCondition> getCriterias()
	{
		return criterias;
	}

	/**
	 * @param criterias the criterias to set
	 */
	public void setCriterias(List<SearchCondition> criterias)
	{
		this.criterias = criterias;
	}

	public String createCondition()
	{
		return createCondition("");
	}

	public String createCondition(String prefix)
	{
		String tableName = "";
		if (null != prefix && !prefix.equals(""))
			tableName = prefix + ".";

		StringBuilder sb = new StringBuilder();
		if (!criterias.isEmpty())
		{
			sb.append(" where");
			int index = 0;
			for (SearchCondition cond : criterias)
			{
				if (cond instanceof SearchCriteria)
				{
					SearchCriteria c = (SearchCriteria) cond;
					if (0 != index)
					{
						if (c.isAnd())
						{
							sb.append(" AND");
						}
						else
						{
							sb.append(" OR");
						}
					}
					if (c.getOp().equals(SearchCriteria.OP_IN) || c.getOp().equals(SearchCriteria.OP_NOT_IN))
					{
						if(c.getValue() instanceof List<?>)
						{

						}
						else
						{
							if (c.getOp().equals(SearchCriteria.OP_IN))
								c.setOp(SearchCriteria.OP_EQUAL);
							else
								c.setOp(SearchCriteria.OP_NOT_EQUAL);

							sb.append( " " + tableName + c.getField() + c.getOp() + ":" + c.getField() + (index++) );
						}
					}
					else
					{
						if (c.getType().equalsIgnoreCase("Date") && c.getOp().equals(SearchCriteria.OP_BETWEEN))
							sb.append( " " + tableName + c.getField() + c.getOp() + ":" + c.getField() + (index) + "s and :" + c.getField() + (index++) + "e");
						else
							sb.append( " " + tableName + c.getField() + c.getOp() + ":" + c.getField() + (index++) );
					}
				}
			}
		}
		return sb.toString();
	}

	public void setParameter(Query query)
	{
		int index = 0;
		for (SearchCondition cond : criterias)
		{
			if (cond instanceof SearchCriteria)
			{
				SearchCriteria c = (SearchCriteria) cond;
				String fieldName = c.getField() + (index++);
				setParameter(query, c, fieldName);
			}
		}
	}

	/**
	* Purpose:
	* @param query
	* @param c
	* @param fieldName
	*/
	private void setParameter(Query query, SearchCriteria c, String fieldName)
	{
		if (null != c.getPath())
		{
			fieldName = c.getPath() + "." + fieldName;
		}
		if (c.getType().equalsIgnoreCase("String"))
		{
			query.setParameter(fieldName, c.getValue().toString());
		}
		else if (c.getType().equalsIgnoreCase("boolean"))
		{
			query.setParameter(fieldName, Boolean.parseBoolean(c.getValue().toString()));
		}
		else if (c.getType().equalsIgnoreCase("long"))
		{
			query.setParameter(fieldName, Long.parseLong(c.getValue().toString()));
		}
		else if (c.getType().equalsIgnoreCase("double"))
		{
			query.setParameter(fieldName, Double.parseDouble(c.getValue().toString()));
		}
		else if (c.getType().equalsIgnoreCase("Date"))
		{
		    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		    dateFormat.setLenient(false);
		    Date date = null;
		    try
			{
				date = dateFormat.parse(c.getValue().toString());
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}
			if (null != date)
			{
				Calendar cal = Calendar.getInstance(Locale.US);
				cal.setTime(date);
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.MILLISECOND, 0);
				query.setParameter(fieldName + "s", cal.getTime());

				cal.add(Calendar.DATE, 1);
				query.setParameter(fieldName + "e", cal.getTime());
			}
		}
	}

	public Query createTotalQuery(EntityManager em)
	{
		return createQuery(em, true);
	}

	public Query createQuery(EntityManager em)
	{
		return createQuery(em, false);
	}

	public Query createQuery(EntityManager em, boolean isTotal)
	{
//		em.createNativeQuery("alter session set NLS_COMP=LINGUISTIC").executeUpdate();
//		em.createNativeQuery("alter session set NLS_SORT=BINARY_CI").executeUpdate();
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<?> cq = cb.createQuery(rootObject);
		CriteriaQuery<Long> cqTotal = cb.createQuery(Long.class);
		Root<?> root;
		Map<String,From<?,?>> froms = new Hashtable<String,From<?,?>>();

		if(isTotal)
		{
			root = cqTotal.from(rootObject);
			//if (null != root.getModel().getIdType())
//			if (null != root.getModel().getIdType() && !(root.getModel().getIdType() instanceof BasicType) && !root.getModel().getIdType().getJavaType().getName().startsWith("java"))
//				if(distinct){
//					cqTotal.select(cb.countDistinct(root.get(root.getModel().getAttributes().iterator().next().getName())));
//				}else{
//					cqTotal.select(cb.count(root.get(root.getModel().getAttributes().iterator().next().getName())));
//				}
//			else
				if(distinct){
					cqTotal.select(cb.countDistinct(root));
				}else{
					cqTotal.select(cb.count(root));
				}
		}
		else
		{
			root = cq.from(rootObject);
			cq.distinct(distinct);
			if (null != select)
			{
				CriteriaQuery<Tuple> cqSelect = cb.createTupleQuery();
				root = cqSelect.from(rootObject);
				Selection<?> tuple = null;
				List<Selection<?>> tupleList = new ArrayList<Selection<?>>();

				for(String selectItem : select){
					StringTokenizer st = new StringTokenizer(selectItem, ".");
					String field = st.nextToken();

					From<?,?> from = root;

					while (st.hasMoreTokens())
					{
						if (froms.containsKey(field))
						{
							from = froms.get(field);
						}else{
							from = from.join(field, JoinType.LEFT);
							froms.put(field, from);
						}
						field = st.nextToken();
					}

					tuple = from.get(field);
					tupleList.add(tuple);
				}
				if (null == sort)
					addSort(select.get(0), true);

				cqSelect.multiselect(tupleList);
				cqSelect.distinct(distinct);
				cq = cqSelect;
			}
		}

		Predicate predicate = null;
		if(!criterias.isEmpty())
		{
			for (SearchCondition cond : criterias)
			{
				From<?,?> from = root;

				if (cond instanceof SearchCriteria)
				{
					SearchCriteria c = (SearchCriteria) cond;

					if(null != c.getPath())
					{
						StringTokenizer st = new StringTokenizer(c.getPath(), ".");
						while (st.hasMoreTokens())
						{
							//path = root.get(st.nextToken());
							String fromTable = st.nextToken();
							if (froms.containsKey(fromTable))
							{
								from = froms.get(fromTable);
							}
							else
							{
								from = from.join(fromTable, JoinType.LEFT);
								froms.put(fromTable, from);
							}
						}
					}
				}

				Predicate p = cond.createPredicate(cb, from);

				if(null == predicate)
				{
					predicate = cb.and(p);
				}
				else
				{
					if (cond.isAnd())
						predicate = cb.and(predicate, p);
					else
						predicate = cb.or(predicate, p);
				}
			}
		}

		if(isTotal)
		{
			if(null != predicate)
			{
				cqTotal.where(predicate);
			}

			TypedQuery<Long> query = em.createQuery(cqTotal);
			return query;
		}
		if(null != predicate)
		{
			cq.where(predicate);
		}

		if (null != sort)
		{
			for (OrderBy order : sort)
			{
				StringTokenizer st = new StringTokenizer(order.getField(), ".");
				String field = st.nextToken();
				// Path<?> p = root;
				From<?, ?> from = root;
				while (st.hasMoreTokens())
				{
					// path = root.get(st.nextToken());
					if (froms.containsKey(field))
					{
						from = froms.get(field);
					}
					else
					{
						from = from.join(field, JoinType.LEFT);
					}
					field = st.nextToken();
				}
				if (order.isAsc())
				{
					cq.orderBy(cb.asc(from.get(field)));
				}
				else
				{
					cq.orderBy(cb.desc(from.get(field)));
				}

			}
		}

		Query query = em.createQuery(cq);

		if(paging)
		{
			query.setFirstResult(first);
			query.setMaxResults(pageSize);
		}

		query.setHint("org.hibernate.cacheable", true);

		return query;
	}

	public static Query createSelectAllQuery(EntityManager em, Class<?> classx)
	{
		return createSelectAllQuery(em, classx, "id");
	}

	public static Query createSelectAllQuery(EntityManager em, Class<?> classx, String sort)
	{
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<?> cq = cb.createQuery(classx);
		Root<?> root = cq.from(classx);

		if (null != sort)
		{
			try
			{
				cq.orderBy(cb.asc(root.get(sort)));
			}
			catch (Exception e)
			{
			}
		}

		Query query = em.createQuery(cq);

		query.setHint("org.hibernate.cacheable", true);

		return query;
	}

	/**
	 * @return the criteria
	 */
	public SearchCriteria getCriteria()
	{
		return criteria;
	}

	/**
	 * @param criteria the criteria to set
	 */
	public void setCriteria(SearchCriteria criteria)
	{
		this.criteria = criteria;
	}

	/**
	 * @return the page
	 */
	public int getPage()
	{
		return (first / pageSize)+1;
	}

	/**
	 * @param page the page to set
	 */
	public void setPage(int page)
	{
		this.first = (page - 1) * pageSize;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize()
	{
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}

	/**
	 * @return the paging
	 */
	public boolean isPaging()
	{
		return paging;
	}

	/**
	 * @param paging the paging to set
	 */
	public void setPaging(boolean paging)
	{
		this.paging = paging;
	}

	/**
	 * @return the first
	 */
	public int getFirst()
	{
		return first;
	}

	/**
	 * @param first the first to set
	 */
	public void setFirst(int first)
	{
		this.first = first;
	}

	/**
	 * @return the sorts
	 */
	public List<OrderBy> getSort()
	{
		return sort;
	}

	/**
	 * @param sorts the sorts to set
	 */
	public void setSort(List<OrderBy> sorts)
	{
		this.sort = sorts;
	}

	public void addSort(String field, String asc)
	{
		if (null == sort)
		{
			sort = new ArrayList<OrderBy>();
		}
		sort.add(new OrderBy(field, asc));
	}

	public void addSort(String field, boolean asc)
	{
		if (null == sort)
		{
			sort = new ArrayList<OrderBy>();
		}
		sort.add(new OrderBy(field, asc));
	}

	public void addSort(String field, boolean asc, int index)
	{
		if (null == sort || sort.size() < index)
		{
			addSort(field, asc);
		}
		sort.add(index, new OrderBy(field, asc));
	}


	/**
	 * @return the asc
	 */
	public boolean isAsc()
	{
		return asc;
	}

	/**
	 * @param asc the asc to set
	 */
	public void setAsc(boolean asc)
	{
		this.asc = asc;
	}

	public void addConditionPeriodDate(String path,Object from, Object to)
	{
		if (null != from && null != to)
		{
			SearchCriterias scDate = new SearchCriterias(rootObject);
			SearchCriteria c = new SearchCriteria();
		/*
			Case 1     O---------O
			Case 2				           O---------O
			Case 3               O---------O
			Case 4     O----------------------------O
					      From                 To
		*/
			//Case 1
			SearchCriterias scDateCase1 = new SearchCriterias(rootObject);
			c = new SearchCriteria();
			c.setPath(path);
			c.setField("dateFrom");
			c.setOp(SearchCriteria.OP_BETWEEN);
			c.setValue(DateUtility.getStartDate((Date) from));
			c.setValue2(DateUtility.getEndDate((Date) to));
			scDateCase1.setAnd(false);
			scDateCase1.getCriterias().add(c);

			scDate.getCriterias().add(scDateCase1);

			//Case 2
			SearchCriterias scDateCase2 = new SearchCriterias(rootObject);
			c = new SearchCriteria();
			c.setPath(path);
			c.setField("dateTo");
			c.setOp(SearchCriteria.OP_BETWEEN);
			c.setValue(DateUtility.getStartDate((Date) from));
			c.setValue2(DateUtility.getEndDate((Date) to));
			scDateCase2.setAnd(false);
			scDateCase2.getCriterias().add(c);

			scDate.getCriterias().add(scDateCase2);

			//Case 3
			SearchCriterias scDateCase3 = new SearchCriterias(rootObject);
			c = new SearchCriteria();
			c.setPath(path);
			c.setField("dateFrom");
			c.setOp(SearchCriteria.OP_GREATERTHAN_OR_EQUAL);
			c.setValue(DateUtility.getStartDate((Date) from));
			scDateCase3.getCriterias().add(c);

			c = new SearchCriteria();
			c.setPath(path);
			c.setField("dateTo");
			c.setOp(SearchCriteria.OP_LESSTHAN_OR_EQUAL);
			c.setValue(DateUtility.getEndDate((Date) to));
			scDateCase3.setAnd(false);
			scDateCase3.getCriterias().add(c);

			scDate.getCriterias().add(scDateCase3);

			//Case 4
			SearchCriterias scDateCase4 = new SearchCriterias(rootObject);
			c = new SearchCriteria();
			c.setPath(path);
			c.setField("dateFrom");
			c.setOp(SearchCriteria.OP_LESSTHAN_OR_EQUAL);
			c.setValue(DateUtility.getStartDate((Date) from));
			scDateCase4.getCriterias().add(c);

			c = new SearchCriteria();
			c.setPath(path);
			c.setField("dateTo");
			c.setOp(SearchCriteria.OP_GREATERTHAN_OR_EQUAL);
			c.setValue(DateUtility.getEndDate((Date) to));
			scDateCase4.setAnd(false);
			scDateCase4.getCriterias().add(c);

			scDate.getCriterias().add(scDateCase4);

			getCriterias().add(scDate);
		}
		else if (null != from && null == to)
		{
			SearchCriteria c = new SearchCriteria();
			c = new SearchCriteria();
			c.setPath(path);
			c.setField("dateFrom");
			c.setOp(SearchCriteria.OP_LESSTHAN_OR_EQUAL);
			c.setValue(DateUtility.getStartDate((Date) from));
			getCriterias().add(c);

			c = new SearchCriteria();
			c.setPath(path);
			c.setField("dateTo");
			c.setOp(SearchCriteria.OP_GREATERTHAN_OR_EQUAL);
			c.setValue(DateUtility.getStartDate((Date) from));
			getCriterias().add(c);
		}
		else if (null == from && null != to)
		{
			SearchCriteria c = new SearchCriteria();
			c = new SearchCriteria();
			c.setPath(path);
			c.setField("dateFrom");
			c.setOp(SearchCriteria.OP_LESSTHAN_OR_EQUAL);
			c.setValue(DateUtility.getEndDate((Date) to));
			getCriterias().add(c);

			c = new SearchCriteria();
			c.setPath(path);
			c.setField("dateTo");
			c.setOp(SearchCriteria.OP_GREATERTHAN_OR_EQUAL);
			c.setValue(DateUtility.getEndDate((Date) to));
			getCriterias().add(c);
		}
	}

	public void addCondition(String field, String operation, Object value)
	{
		SearchCriteria c = new SearchCriteria();
		c.setField(field);
		c.setOp(operation);
		c.setValue(value);
		getCriterias().add(c);
	}

	public void addCondition(String path, String field, SearchCriteriaOperation operation, Object value)
	{
		addCondition(path, field, operation, value, true);
	}
	public void addCondition(String path, String field, SearchCriteriaOperation operation, Object value, boolean isAnd)
	{
		SearchCriteria c = new SearchCriteria();
		c.setPath(path);
		c.setField(field);
		c.setOp(operation);
		c.setValue(value);
		c.setAnd(isAnd);
		getCriterias().add(c);
	}

	public void addCondition(String field, SearchCriteriaOperation operation, Object value)
	{
		if(!field.contains(".")){
			addCondition(field, operation, value, true);
		}else{
			int idx = field.lastIndexOf(".");
			addCondition(field.substring(0, idx), field.substring(idx + 1), operation, value, true);
		}
	}

	public void addCondition(String field, SearchCriteriaOperation operation, Object value, boolean isAnd)
	{
		SearchCriteria c = new SearchCriteria();
		c.setField(field);
		c.setOp(operation);
		c.setValue(value);
		c.setAnd(isAnd);
		getCriterias().add(c);
	}

	public void addCondition(String field, SearchCriteriaOperation operation, Object value, Object value2, boolean isAnd)
	{
		SearchCriteria c = new SearchCriteria();
		if(!field.contains(".")){
			c.setField(field);
		}else{
			int idx = field.lastIndexOf(".");
			c.setField(field.substring(idx + 1));
			c.setPath(field.substring(0, idx));
		}
		c.setOp(operation);
		c.setValue(value);
		c.setValue2(value2);
		c.setAnd(isAnd);
		getCriterias().add(c);
	}

	/**
	 * @return the rootObject
	 */
	public Class<?> getRootObject()
	{
		return rootObject;
	}

	public Predicate createPredicate(CriteriaBuilder cb, Path<?> root) {
		//Root<?> root = cq.from(rootObject);

		Map<String,From<?,?>> froms = new Hashtable<String,From<?,?>>();

		Predicate predicate = null;

		for (SearchCondition cond : criterias)
		{
			Predicate p = null;
			if (cond instanceof SearchCriteria)
			{
				SearchCriteria c = (SearchCriteria) cond;
				From<?,?> from = (From<?,?>)root;
				if(null != c.getPath())
				{
					StringTokenizer st = new StringTokenizer(c.getPath(), ".");
					while (st.hasMoreTokens())
					{
						//path = root.get(st.nextToken());
						String fromTable = st.nextToken();
						if (froms.containsKey(fromTable))
						{
							from = froms.get(fromTable);
						}
						else
						{
							from = from.join(fromTable, JoinType.LEFT);
							froms.put(fromTable, from);
						}
					}
				}
				p = c.createPredicate(cb, from);
			}
			if (cond instanceof SearchCriterias)
			{
				p = cond.createPredicate(cb, root);
			}
			if(null == predicate)
			{
				predicate = cb.and(p);
			}
			else
			{
				if (cond.isAnd())
					predicate = cb.and(predicate, p);
				else
					predicate = cb.or(predicate, p);
			}
		}

		return predicate;
	}


	/**
	 * @return the and
	 */
	public boolean isAnd()
	{
		return and;
	}

	/**
	 * @param and the and to set
	 */
	public void setAnd(boolean and)
	{
		this.and = and;
	}

	/**
	 * @return the distinct
	 */
	public boolean isDistinct()
	{
		return distinct;
	}

	/**
	 * @param distinct the distinct to set
	 */
	public void setDistinct(boolean distinct)
	{
		this.distinct = distinct;
	}

	/**
	 * @return the select
	 */
	public List<String> getSelect() {
		return select;
	}

	/**
	 * @param select the select to set
	 */
	public void setSelect(List<String> select) {
		this.select = select;
	}
}
