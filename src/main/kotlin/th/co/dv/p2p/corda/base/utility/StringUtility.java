package th.co.dv.p2p.corda.base.utility;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;

public class StringUtility
{
	@SuppressWarnings("unused")
	private static Logger logger = LoggerFactory.getLogger(StringUtility.class);

	public static String formatStringFixLength(String str, char fill, int length)
	{
		return formatStringFixLength(str, fill, length, false);
	}

	public static String formatStringFixLength(String str, char fill, int length, boolean isAppend)
	{
		if (str == null) str = "";
		if (length <= 0) return str;
		if (length < str.length()) return str.substring(str.length() - length);

		StringBuilder result = new StringBuilder();
		if (null != str) result = new StringBuilder(str);

		while (result.length() < length)
		{
			if (isAppend)
				result.append(fill);
			else
				result.insert(0, fill);
		}
		return result.toString();
	}

	public static boolean isEmpty(String str)
	{
		if (null == str || str.trim().length() == 0) return true;

		return false;
	}

	public static String convertFromUTF8(String s)
	{
		String out = null;
		try
		{
			out = new String(s.getBytes("ISO-8859-1"), "UTF-8");
		}
		catch (java.io.UnsupportedEncodingException e)
		{
			return null;
		}
		return out;
	}

	// convert from internal Java String format -> UTF-8
	public static String convertToUTF8(String s)
	{
		String out = null;
		try
		{
			out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
		}
		catch (java.io.UnsupportedEncodingException e)
		{
			return null;
		}
		return out;
	}

	public static String escaseHtml(String strInput)
	{
		return StringEscapeUtils.escapeHtml4(strInput == null ? "" : strInput);
	}

	public static String rpad(String s, int n)
	{
		return String.format("%1$-" + n + "s", s);
	}

	public static String lpad(String s, int n)
	{
		return String.format("%1$" + n + "s", s);
	}

	public static String lpad(String str, String padding, int i)
	{
		String newStr = str;
		if (isEmpty(newStr))
			newStr = "";
		while (i > newStr.length())
		{
			newStr = padding + newStr;
		}

		return newStr;
	}

	public static String rpad(String str, String padding, int i)
	{
		String newStr = str;
		if (isEmpty(newStr))
			newStr = "";
		while (i > newStr.length())
		{
			newStr = newStr + padding;
		}

		return newStr;
	}

	public static String showResult(String str)
	{
		if (isEmpty(str))
		{
			str = "";
		}
		return str;
	}

	// public static void main(String args[]){
	// String s = "0000000111|0000000113|0000000112";
	// String[] ss = s.split("\\|");
	// //logger.info("ss size : " + ss.length);
	// for(String a : ss){
	// //logger.info("a : " + a);
	// }
	// }

	public static String cvtFullNumber(int number, int lenght)
	{
		return StringUtils.leftPad(Integer.toString(number), lenght, "0");
	}

	public static String cvtFullNumber(String number, int lenght)
	{
		return StringUtils.leftPad(number, lenght, "0");
	}

	public static String leftPadWithChar(String str, char del)
	{

		if (!isEmpty(str))
		{
			int legth = str.length();
			for (int i = 0; i < legth; i++)
			{
				if (str.charAt(0) == del)
				{
					str = str.substring(1, str.length());
				}
				else
				{
					break;
				}
			}
		}
		return str;
	}

	public static String getServiceNameFromSOAP(String sendingMessage, String indexStart)
	{
		String functionName = "";
		try
		{
			String sendingMessageIndexOf = sendingMessage.substring(sendingMessage.indexOf(indexStart) + indexStart.length());
			functionName = sendingMessageIndexOf.substring(0, sendingMessageIndexOf.indexOf(" "));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return functionName;
	}

	public static String checkNull(String str){
		return StringUtils.isNotEmpty(str) ? str : "";
	}

	public static String toString(Long l){
		return l != null ? l.toString() : "";
	}

	public static String convertFormatDate(String input, String asIsFormat, String toBeFormat){
		SimpleDateFormat asIs = new SimpleDateFormat(asIsFormat);
		SimpleDateFormat toBe = new SimpleDateFormat(toBeFormat);
		String newFormat = "";
		try {
			if(!isEmpty(input) && !isEmpty(asIsFormat) && !isEmpty(toBeFormat)){
				newFormat = toBe.format(asIs.parse(input));
			}
		} catch (Exception e) {
		    e.printStackTrace();
		}
		return newFormat;
	}
}
