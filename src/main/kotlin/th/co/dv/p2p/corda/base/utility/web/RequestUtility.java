package th.co.dv.p2p.corda.base.utility.web;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import th.co.dv.p2p.corda.base.utility.StringUtility;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RequestUtility
{
	public static String getCurrentPage()
	{
		HttpServletRequest request = getCurrentRequest();

		String path = request.getServletPath();
		if (!StringUtility.isEmpty(request.getQueryString()))
		{
			path = path + "?" + request.getQueryString();
		}
		return path;
	}

	public static HttpServletRequest getCurrentRequest()
	{
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		if (null == request)
		{
			throw new UnsupportedOperationException("Request is not set.");
		}
		return request;
	}

	public static HttpSession getCurrentSession()
	{
		return getCurrentRequest().getSession();
	}

	public static String getContextPath()
	{
		HttpServletRequest request = getCurrentRequest();((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		String contextPath = request.getContextPath();
		return contextPath;
	}
	public static String getContextUrl(String path)
	{
		String contextUrl = path;

		HttpServletRequest request = getCurrentRequest();
		if (path.startsWith("/"))
		{
			contextUrl = request.getContextPath() + contextUrl;
		}
		else
		{
			contextUrl = request.getContextPath() + "/" + contextUrl;
		}
		return contextUrl;
	}
}
