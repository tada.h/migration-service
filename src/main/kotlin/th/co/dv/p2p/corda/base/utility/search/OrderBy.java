/**
* <pre><b>
*	Version 	Date		Time		Developer	Remark</b>
*	001		Mar 28, 2011	10:40:26 AM	chatchch
* </pre>
*/
package th.co.dv.p2p.corda.base.utility.search;

/**
 * @author chatchch
 *
 */
public class OrderBy
{
	private String field;
	private boolean asc;

	public OrderBy(String field, String asc)
	{
		this (field,"asc".equalsIgnoreCase(asc));
	}

	public OrderBy( String field, boolean asc )
	{
		this.field = field;
		this.asc = asc;
	}

	/**
	 * @return the field
	 */
	public String getField()
	{
		return field;
	}
	/**
	 * @param field the field to set
	 */
	public void setField(String field)
	{
		this.field = field;
	}
	/**
	 * @return the asc
	 */
	public boolean isAsc()
	{
		return asc;
	}
	/**
	 * @param asc the asc to set
	 */
	public void setAsc(boolean asc)
	{
		this.asc = asc;
	}

	public String createQuery()
	{
		return createQuery(null);
	}

	public String createQuery(String prefix)
	{
		StringBuilder sb = new StringBuilder();
		sb.append (" ");
		if (null != prefix)
		{
			sb.append (prefix + ".");
		}
		sb.append(field);
		if(asc)
		{
			sb.append(" ASC");
		}
		else
		{
			sb.append(" DESC");
		}

		return sb.toString();
	}

}
