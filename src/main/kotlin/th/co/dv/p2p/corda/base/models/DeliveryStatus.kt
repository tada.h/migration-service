package th.co.dv.p2p.corda.base.models

enum class DeliveryStatus(val code: String?, val description: String?) {
    COMPLETED("X","Yes");

    companion object {
        fun fromCode(code: String?) = DeliveryStatus.values().filter { it.code == code }
        fun fromDescription(description: String?) = DeliveryStatus.values().filter { it.description == description }
    }
}