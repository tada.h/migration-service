/**
* Copyright (c) 2000-2009 IT One Co., Ltd. All rights reserved.
* Purpose:
* @author	chatchch
* @version
* @copyright	IT One Co., Ltd.
* <pre><b>
*	Version 	Date		Time		Developer	Remark</b>
*	001		Oct 16, 2010	12:36:21 PM	chatchch
* </pre>
*/
package th.co.dv.p2p.corda.base.utility.search.paging;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import th.co.dv.p2p.corda.base.utility.search.SearchCriterias;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

// import com.fasterxml.jackson.databind.annotation.Json;


/**
 *
 */
// @JsonSerialize(using = ListSerializer.class)
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public class PagableList<T> implements List<T>
{
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 72746627122448495L;

	@JsonProperty
	private int page;
	private int pageSize;
	private String orderBy;
	private boolean asc;
	private int totalSize;
	private List<T> list;
	private EntityManager entityManager;
	private SearchCriterias searchCriterias;
	private boolean isDeferFetch;

	public PagableList(List<T> list)
	{
		this.list = list;

		pageSize = list.size();
//		totalSize = list.size();
	}

	public PagableList(SearchCriterias searchCriterias, EntityManager entityManager)
	{
		this.searchCriterias = searchCriterias;
		this.entityManager = entityManager;
		this.isDeferFetch = true;
	}

	/**
	 * @return the data
	 */
	@SuppressWarnings("unchecked")
	public List<T> getData()
	{
		if(this.isDeferFetch && searchCriterias != null && entityManager != null){
			PagableList<T> pageList = null;
			System.out.println("Page : " + this.page + ", PageSize : " + this.pageSize);
			if(this.pageSize > 0){
				pageList = PaginationHelper.fetch(searchCriterias, this.page, this.pageSize, orderBy, asc, entityManager);
			}else{
				Query query = searchCriterias.createQuery(entityManager);
				pageList = new PagableList<T>(query.getResultList());
			}

			if(pageList != null){
				this.setData(pageList.getData());
				this.setTotalSize(pageList.getTotalSize());
			}
		}

		return this.list;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(List<T> data)
	{
		this.list = data;
	}
	/**
	 * @return the pageSize
	 */
	public int getPageSize()
	{
		return pageSize;
	}
	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}
	/**
	 * @return the totalSize
	 */
	public int getTotalSize()
	{
		return totalSize;
	}
	/**
	 * @param totalSize the totalSize to set
	 */
	public void setTotalSize(int totalSize)
	{
		this.totalSize = totalSize;
	}

	/**
	 * @return the partialList
	 */
	public boolean getPartialList()
	{
		return (totalSize!=list.size());
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public boolean isAsc() {
		return asc;
	}

	public void setAsc(boolean asc) {
		this.asc = asc;
	}

	public boolean isDeferFetch() {
		return isDeferFetch;
	}

	public void setDeferFetch(boolean isDeferFetch) {
		this.isDeferFetch = isDeferFetch;
	}

	/**
	 * @param index
	 * @param element
	 * @see List#add(int, Object)
	 */
	public void add(int index, T element)
	{
		list.add(index, element);
	}

	/**
	 * @param o
	 * @return
	 * @see List#add(Object)
	 */
	public boolean add(T o)
	{
		return list.add(o);
	}

	/**
	 * @param c
	 * @return
	 * @see List#addAll(Collection)
	 */
	public boolean addAll(Collection<? extends T> c)
	{
		return list.addAll(c);
	}

	/**
	 * @param index
	 * @param c
	 * @return
	 * @see List#addAll(int, Collection)
	 */
	public boolean addAll(int index, Collection<? extends T> c)
	{
		return list.addAll(index, c);
	}

	/**
	 *
	 * @see List#clear()
	 */
	public void clear()
	{
		list.clear();
	}

	/**
	 * @param o
	 * @return
	 * @see List#contains(Object)
	 */
	public boolean contains(Object o)
	{
		return list.contains(o);
	}

	/**
	 * @param c
	 * @return
	 * @see List#containsAll(Collection)
	 */
	public boolean containsAll(Collection<?> c)
	{
		return list.containsAll(c);
	}

	/**
	 * @param o
	 * @return
	 * @see List#equals(Object)
	 */
	public boolean equals(Object o)
	{
		return list.equals(o);
	}

	/**
	 * @param index
	 * @return
	 * @see List#get(int)
	 */
	public T get(int index)
	{
		return list.get(index);
	}

	/**
	 * @return
	 * @see List#hashCode()
	 */
	public int hashCode()
	{
		return list.hashCode();
	}

	/**
	 * @param o
	 * @return
	 * @see List#indexOf(Object)
	 */
	public int indexOf(Object o)
	{
		return list.indexOf(o);
	}

	/**
	 * @return
	 * @see List#isEmpty()
	 */
	public boolean isEmpty()
	{
		return list.isEmpty();
	}

	/**
	 * @return
	 * @see List#iterator()
	 */
	public Iterator<T> iterator()
	{
		return list.iterator();
	}

	/**
	 * @param o
	 * @return
	 * @see List#lastIndexOf(Object)
	 */
	public int lastIndexOf(Object o)
	{
		return list.lastIndexOf(o);
	}

	/**
	 * @return
	 * @see List#listIterator()
	 */
	public ListIterator<T> listIterator()
	{
		return list.listIterator();
	}

	/**
	 * @param index
	 * @return
	 * @see List#listIterator(int)
	 */
	public ListIterator<T> listIterator(int index)
	{
		return list.listIterator(index);
	}

	/**
	 * @param index
	 * @return
	 * @see List#remove(int)
	 */
	public T remove(int index)
	{
		return list.remove(index);
	}

	/**
	 * @param o
	 * @return
	 * @see List#remove(Object)
	 */
	public boolean remove(Object o)
	{
		return list.remove(o);
	}

	/**
	 * @param c
	 * @return
	 * @see List#removeAll(Collection)
	 */
	public boolean removeAll(Collection<?> c)
	{
		return list.removeAll(c);
	}

	/**
	 * @param c
	 * @return
	 * @see List#retainAll(Collection)
	 */
	public boolean retainAll(Collection<?> c)
	{
		return list.retainAll(c);
	}

	/**
	 * @param index
	 * @param element
	 * @return
	 * @see List#set(int, Object)
	 */
	public T set(int index, T element)
	{
		return list.set(index, element);
	}

	/**
	 * @return
	 * @see List#size()
	 */
	public int size()
	{
		return list.size();
	}

	/**
	 * @param fromIndex
	 * @param toIndex
	 * @return
	 * @see List#subList(int, int)
	 */
	public List<T> subList(int fromIndex, int toIndex)
	{
		return list.subList(fromIndex, toIndex);
	}

	/**
	 * @return
	 * @see List#toArray()
	 */
	public Object[] toArray()
	{
		return list.toArray();
	}

	/**
	 * @param <T>
	 * @param a
	 * @return
	 * @see List#toArray(T[])
	 */
	@SuppressWarnings("hiding")
	public <T> T[] toArray(T[] a)
	{
		return list.toArray(a);
	}

	public String toString()
	{
		return "{page: " + page + ", pageSize: " + pageSize + ", totalSize: " + totalSize + ", list: " + list.toString() + "}";
	}
}
