package th.co.dv.p2p.corda.base.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.web.context.request.RequestContextHolder;
import th.co.dv.p2p.corda.base.utility.StringUtility;
import th.co.dv.p2p.corda.base.utility.search.*;
import th.co.dv.p2p.corda.base.utility.search.paging.PagableList;
import th.co.dv.p2p.corda.base.utility.web.RequestUtility;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <pre><b>
 *	Version 	Date		Time		Developer	Remark</b>
 *	001		Feb 19, 2014	2:21:35 PM	chatchch
 * </pre>
 */

/**
 * @author chatchch
 *
 */
public class CustomJpaRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> implements CustomJpaRepository<T, ID>
{
	protected Logger logger = LoggerFactory.getLogger(getClass());

	private EntityManager entityManager;

	/**
	 * @return the entityManager
	 */
	public EntityManager getEntityManager()
	{
		return entityManager;
	}

	// There are two constructors to choose from, either can be used.
	public CustomJpaRepositoryImpl(Class<T> domainClass, EntityManager entityManager)
	{
		super(domainClass, entityManager);

		// This is the recommended method for accessing inherited class
		// dependencies.
		this.entityManager = entityManager;
	}

	private void setPaging(SearchCriterias criterias)
	{
		if (logger.isTraceEnabled())
			logger.trace("Trying to get paging information from request");


		if (null == RequestContextHolder.getRequestAttributes())
			return;

		String page = null;
		String rows = null;
		String pageParam = "page";
		String pageSizeParam = "pageSize";
		try
		{
			page = RequestUtility.getCurrentRequest().getParameter(pageParam);
			rows = RequestUtility.getCurrentRequest().getParameter(pageSizeParam);
			if (!StringUtility.isEmpty(page) && !StringUtility.isEmpty(rows))
			{
				if (logger.isDebugEnabled())
					logger.debug("Set paging[" + page + "," + rows + "]");
				criterias.setPaging(!rows.equals("-1"));
				criterias.setPageSize(Integer.parseInt(rows));
				criterias.setPage(Integer.parseInt(page));
			}
		}
		catch (Exception e)
		{
			if (logger.isErrorEnabled())
				logger.error("Error when setting paging to search criteria[" + page + "," + rows + "]: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private void setSorting(SearchCriterias criterias)
	{
		if (null == RequestContextHolder.getRequestAttributes())
			return;

		String sort = null;
		String order = null;
		try
		{
			sort = RequestUtility.getCurrentRequest().getParameter("sortField");
			order = RequestUtility.getCurrentRequest().getParameter("sortOrder");
			if (!StringUtility.isEmpty(sort) && !StringUtility.isEmpty(order))
			{
				if (logger.isDebugEnabled())
					logger.debug("Add sorting[" + sort + " " + order + "]");
				criterias.addSort(sort, order.equalsIgnoreCase("asc") || order.equalsIgnoreCase("1"), 0);
			}
		}
		catch (Exception e)
		{
			if (logger.isDebugEnabled())
				logger.debug("Error when setting sorting to search criteria[" + sort + "," + order +"]: " + e.getMessage());
		}
	}

	private void addCriteriaFromMap(SearchCriterias criterias, Map<String, Object> param, Map<String, String> operation)
	{
		BeanWrapper bean = new BeanWrapperImpl(criterias.getRootObject());

		param.keySet().forEach(key -> {
			if (isReadableProperty(bean, key)) {
				logger.debug("Readable Property: " + key);
				Object value = param.get(key);
				if (!StringUtility.isEmpty(key)) {
					String oper = operation.containsKey(key)?operation.get(key):"EQUAL";
					SearchCriteriaOperation scOper = SearchCriteriaOperation.EQUAL;
					try {
						scOper = SearchCriteriaOperation.valueOf(oper);
					} catch(Exception ex) {
					}
					String k1 = key;
					Object v1 = value;
					if (key.toLowerCase().endsWith("from")) {
						k1 = key.substring(0, key.length() - 4);
						if (!(v1 instanceof Date)) {
							v1 = this.parseDate(v1.toString());
						}
					} else if (key.toLowerCase().endsWith("to")) {
						k1 = key.substring(0, key.length() - 2);
						if (!(v1 instanceof Date)) {
							v1 = this.parseDate(v1.toString());
						}
					}
						criterias.addCondition(k1, scOper, v1);
				}
			} else {
				logger.debug("Unreadable Property: " + key);
			}
		});
	}

	private Date parseDate(String value)
	{
		try {
			return (new SimpleDateFormat("dd/MM/yyyy")).parse(value.toString());
		} catch(ParseException ex) {
			return null;
		}
	}

	private boolean isReadableProperty(BeanWrapper bean, String key) {
			BeanWrapper b = bean;
			String[] keys = key.split("\\.");
			logger.debug("Checking: " + keys);
			for (String k : keys) {
				logger.debug("Checking: " + k);
				String k2 = k;
				if (k2.toLowerCase().endsWith("from")) {
					k2 = k2.substring(0, k2.length() - 4);
				} else if (k2.toLowerCase().endsWith("to")) {
					k2 = k2.substring(0, k2.length() - 2);
				}
				if (b.isReadableProperty(k2)) {
					logger.info(b.getPropertyTypeDescriptor(k2).getName());
					if (!b.getPropertyTypeDescriptor(k2).isPrimitive() && !b.getPropertyTypeDescriptor(k2).getName().equalsIgnoreCase("java.lang.boolean") && !key.endsWith(k)) {
						logger.info("(b.getPropertyType(k2) : " + b.getPropertyType(k2));
						b = new BeanWrapperImpl(b.getPropertyType(k2));
					}
				} else {
					return false;
				}
			}
			return true;
	}

	private void addCriteriaFromAuthorization(SearchCriterias criterias)
	{

	}

	@SuppressWarnings("unchecked")
	public List<T> list(SearchCriterias criterias, Map<String, Object> param, Map<String, String> operation)
	{
		addCriteriaFromMap(criterias, param, operation);
		addCriteriaFromAuthorization(criterias);
		setPaging(criterias);
		setSorting(criterias);

		Query query = criterias.createQuery(getEntityManager());

		PagableList<T> list = new PagableList<T>(query.getResultList());
		if (criterias.isPaging() && (list.size() == criterias.getPageSize() || criterias.getPage() >= 1))
		{
			list.setPageSize(criterias.getPageSize());
			list.setPage(criterias.getPage());
			list.setTotalSize(count(criterias));
		}
		else
		{
//			list.setTotalSize(count(criterias));
//			list.setPageSize(criterias.getPageSize());
			list.setPage(1);
			list.setTotalSize(count(criterias));
//			list.setTotalSize(list.getData().size());
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<T> list(SearchCriterias criterias, Map<String, Object> param)
	{
		return list(criterias, param, new HashMap());
	}

	@SuppressWarnings("unchecked")
	public List<T> list(SearchCriterias criterias)
	{
		return list(criterias, new HashMap(), new HashMap());
	}

	public int count(SearchCriterias criterias)
	{
		@SuppressWarnings("unchecked")
		TypedQuery<Long> query = (TypedQuery<Long>) criterias.createTotalQuery(getEntityManager());
		return query.getSingleResult().intValue();
	}
}
