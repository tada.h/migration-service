package th.co.dv.p2p.corda.base.utility;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class DateUtility
{
	public static final int RETURN_TYPE_DAY = (1000 * 60 * 60 * 24);

	public static final String DEFAULT_FORMAT = "dd/MM/yyyy";
	public static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";
	public static final String DEFAULT_TIME_STAMP_FORMAT = "dd/MM/yyyy HH:mm:ss";
	public static final String DATE_INT_ID = "yyyyMMddHHmmssSSS";
	public static final String WS_DATE_FORMAT = "yyyy-MM-dd";
	public static final String WS_TIME_FORMAT = "HH:mm:ss";
	public static final String WS_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";;
	public static final DateFormat DATE_TIME_TH = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", new Locale("th", "th"));
	public static final DateFormat DATE_TIME_EN = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
	public static final DateFormat DATE_TH = new SimpleDateFormat("dd/MM/yyyy", new Locale("th", "th"));
	public static final DateFormat DATE_EN = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

	public static Calendar getCurrent()
	{
		Calendar now = Calendar.getInstance(Locale.US);
		return now;
	}

	public static Calendar getCalendar(Date date)
	{
		Calendar now = getCurrent();
		now.setTime(date);
		return now;
	}

	public static Date getCurrentDateTime()
	{
		Calendar now = getCurrent();
		return now.getTime();
	}

	public static Date getCurrentDate()
	{
		Calendar now = getCurrent();
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);
		return now.getTime();
	}

	public static Date getStartDate(Date date)
	{
		Calendar cal = getCalendar(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Date getEndDate(Date date)
	{
		Calendar cal = getCalendar(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal.getTime();
	}

	public static Calendar addDay(Calendar cal, int day)
	{
		cal.add(Calendar.DAY_OF_MONTH, day);
		return cal;
	}

	public static Date addDay(int day)
	{
		return addDay(getCurrent().getTime(), day);
	}

	public static Date addDay(Date date, int day)
	{
		Calendar cal = getCalendar(date);
		return addDay(cal, day).getTime();
	}

	public static Date minusDay(Date date, int day)
	{
		return addDay(date, day * -1);
	}

	public static long getDateTimeDiffInMinute(Date start, Date end)
	{
		Calendar s = getCalendar(start);
		Calendar e = getCalendar(end);

		long diff = e.getTimeInMillis() - s.getTimeInMillis();
		return diff / 60000;
	}

	public static String convertStringSAPToStringDate(String strDate)
	{
		String date = "";
		if (!StringUtility.isEmpty(strDate))
		{
			try
			{
				String strDates[] = strDate.split("-");
				if (strDates != null && strDates.length == 3)
				{
					date = strDates[2] + "/" + strDates[1] + "/" + strDates[0];
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return date;
	}

	private static SimpleDateFormat getDateFormat(String format)
	{
		String dateFormat = DEFAULT_FORMAT;
		if (!StringUtility.isEmpty(format))
		{
			dateFormat = format;
		}
		return new SimpleDateFormat(dateFormat, Locale.US);
	}

	public static Date convertStringToDate(String strDate, String format)
	{
		if (StringUtility.isEmpty(strDate))
			return null;

		SimpleDateFormat sFormat = getDateFormat(format);

		try{
			return sFormat.parse(strDate);
		}catch(ParseException e){
			return null;
		}
	}

	public static Date convertStringToDate(String strDate)
	{
		return convertStringToDate(strDate, DEFAULT_FORMAT);
	}

	public static Date convertStringToDateTime(String strDate, String format)
	{
		if (StringUtility.isEmpty(format))
			return convertStringToDate(strDate, DEFAULT_TIME_STAMP_FORMAT);
		return convertStringToDate(strDate, format);
	}

	public static String convertDateToString(Date date)
	{
		return convertDateToString(date, DEFAULT_FORMAT);
	}

	public static String convertDateToString(Date date, String format)
	{
		if (null == date)
			return "";

		SimpleDateFormat sFormat = getDateFormat(format);
		return sFormat.format(date);
	}

	//public static String convertDateToString(Date date, String format, boolean isBuddistYear){

	//}

	public static String convertDateTimeToTimeString(Date date)
	{
		return convertDateToString(date, DEFAULT_TIME_FORMAT);
	}

	public static String convertDateTimeToString(Date date)
	{
		return convertDateToString(date, DEFAULT_TIME_STAMP_FORMAT);
	}

	public static Date addMonth(Date date, int month)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, month);
		Date resultDate = calendar.getTime();
		return resultDate;
	}

	public static Date addYear(Date date, int year)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, year);
		Date resultDate = calendar.getTime();
		return resultDate;
	}

	public static Date getFirstDateOfMonth(Date date)
	{
		Calendar calendar = getCurrent();
		calendar.setTime(date);
		calendar.set(Calendar.DATE, 1);
		Date firstDate = calendar.getTime();
		return firstDate;
	}

	public static Date getLastDateOfMonth(Date date)
	{
		Date firstDate = getFirstDateOfMonth(date);
		Calendar calendar = getCurrent();
		calendar.setTime(firstDate);
		calendar.add(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE) - 1);
		Date lastDay = calendar.getTime();
		return lastDay;
	}

	public static Date convertStringToDateTime(String date, String time, String dateSeparate, String timeSeparate)
	{
		Calendar cal = getCurrent();
		String[] dates = date.split(dateSeparate);
		String[] times = time.split(timeSeparate);
		cal.set(Integer.parseInt(dates[2]), Integer.parseInt(dates[1]) - 1, Integer.parseInt(dates[0]));
		cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(times[0]));
		cal.set(Calendar.MINUTE, Integer.parseInt(times[1]));
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static String getThaiMonth(String month)
	{
		String thaiMonth = "";
		int monthNo = Integer.parseInt(month);

		switch (monthNo)
		{
			case 1:
				thaiMonth = "มกราคม";
				break;
			case 2:
				thaiMonth = "กุมภาพันธ์";
				break;
			case 3:
				thaiMonth = "มีนาคม";
				break;
			case 4:
				thaiMonth = "เมษายน";
				break;
			case 5:
				thaiMonth = "พฤษภาคม";
				break;
			case 6:
				thaiMonth = "มิถุนายน";
				break;
			case 7:
				thaiMonth = "กรกฎาคม";
				break;
			case 8:
				thaiMonth = "สิงหาคม";
				break;
			case 9:
				thaiMonth = "กันยายน";
				break;
			case 10:
				thaiMonth = "ตุลาคม";
				break;
			case 11:
				thaiMonth = "พฤศจิกายน";
				break;
			case 12:
				thaiMonth = "ธันวาคม";
				break;

		}
		return thaiMonth;
	}

	public static String getShortThaiMonth(String month)
	{
		String thaiMonth = "";
		int monthNo = Integer.parseInt(month);

		switch (monthNo)
		{
			case 1:
				thaiMonth = "ม.ค.";
				break;
			case 2:
				thaiMonth = "ก.พ.";
				break;
			case 3:
				thaiMonth = "มี.ค.";
				break;
			case 4:
				thaiMonth = "เม.ย.";
				break;
			case 5:
				thaiMonth = "พ.ค.";
				break;
			case 6:
				thaiMonth = "มิ.ย.";
				break;
			case 7:
				thaiMonth = "ก.ค.";
				break;
			case 8:
				thaiMonth = "ส.ค.";
				break;
			case 9:
				thaiMonth = "ก.ย.";
				break;
			case 10:
				thaiMonth = "ต.ค.";
				break;
			case 11:
				thaiMonth = "พ.ย.";
				break;
			case 12:
				thaiMonth = "ธ.ค.";
				break;

		}
		return thaiMonth;
	}

	public static String getShortEngMonth(String month)
	{
		String engMonth = "";
		int monthNo = Integer.parseInt(month);

		switch (monthNo)
		{
			case 1:
				engMonth = "Jan";
				break;
			case 2:
				engMonth = "Feb";
				break;
			case 3:
				engMonth = "Mar";
				break;
			case 4:
				engMonth = "Apr";
				break;
			case 5:
				engMonth = "May";
				break;
			case 6:
				engMonth = "Jun";
				break;
			case 7:
				engMonth = "Jul";
				break;
			case 8:
				engMonth = "Aug";
				break;
			case 9:
				engMonth = "Sep";
				break;
			case 10:
				engMonth = "Oct";
				break;
			case 11:
				engMonth = "Nov";
				break;
			case 12:
				engMonth = "Dec";
				break;
		}
		return engMonth;
	}

	public static String convertToThaiFormat(Date date, boolean isFormal)
	{
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
		String dateStr = (dateFormat.format(date)).toString();
		String dateNo = Character.toString(dateStr.charAt(0)) + Character.toString(dateStr.charAt(1));
		String monthNo = Character.toString(dateStr.charAt(3)) + Character.toString(dateStr.charAt(4));
		String year = Character.toString(dateStr.charAt(6)) + Character.toString(dateStr.charAt(7)) + Character.toString(dateStr.charAt(8)) + Character.toString(dateStr.charAt(9));

		int month = Integer.parseInt(monthNo);

		switch (month)
		{
			case 1:
				monthNo = "มกราคม";
				break;
			case 2:
				monthNo = "กุมภาพันธ์";
				break;
			case 3:
				monthNo = "มีนาคม";
				break;
			case 4:
				monthNo = "เมษายน";
				break;
			case 5:
				monthNo = "พฤษภาคม";
				break;
			case 6:
				monthNo = "มิถุนายน";
				break;
			case 7:
				monthNo = "กรกฎาคม";
				break;
			case 8:
				monthNo = "สิงหาคม";
				break;
			case 9:
				monthNo = "กันยายน";
				break;
			case 10:
				monthNo = "ตุลาคม";
				break;
			case 11:
				monthNo = "พฤศจิกายน";
				break;
			case 12:
				monthNo = "ธันวาคม";
				break;
		}
		int yearNo = Integer.parseInt(year);
		yearNo = yearNo + 543;
		if (isFormal == true)
		{
			return dateNo + " " + monthNo + " พ.ศ. " + yearNo;
		}

		return dateNo + " " + monthNo + " " + yearNo;
	}

	public static String convertToThaiShortFormat(Date date, boolean isFormal)
	{
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
		String dateStr = (dateFormat.format(date)).toString();
		String dateNo = Character.toString(dateStr.charAt(0)) + Character.toString(dateStr.charAt(1));
		String monthNo = Character.toString(dateStr.charAt(3)) + Character.toString(dateStr.charAt(4));
		String year = Character.toString(dateStr.charAt(6)) + Character.toString(dateStr.charAt(7)) + Character.toString(dateStr.charAt(8)) + Character.toString(dateStr.charAt(9));
		String thaiMonth = "";

		int month = Integer.parseInt(monthNo);

		switch (month)
		{
			case 1:
				thaiMonth = "ม.ค.";
				break;
			case 2:
				thaiMonth = "ก.พ.";
				break;
			case 3:
				thaiMonth = "มี.ค.";
				break;
			case 4:
				thaiMonth = "เม.ย.";
				break;
			case 5:
				thaiMonth = "พ.ค.";
				break;
			case 6:
				thaiMonth = "มิ.ย.";
				break;
			case 7:
				thaiMonth = "ก.ค.";
				break;
			case 8:
				thaiMonth = "ส.ค.";
				break;
			case 9:
				thaiMonth = "ก.ย.";
				break;
			case 10:
				thaiMonth = "ต.ค.";
				break;
			case 11:
				thaiMonth = "พ.ย.";
				break;
			case 12:
				thaiMonth = "ธ.ค.";
				break;
		}
		int yearNo = Integer.parseInt(year);
		yearNo = yearNo + 543;
		if (isFormal == true)
		{
			return dateNo + " " + thaiMonth + " พ.ศ. " + yearNo;
		}

		return dateNo + " " + thaiMonth + " " + yearNo;
	}

	public static String convertToDBFormat(String dateStr)
	{
		String engMonth = "";
		String dateNo = Character.toString(dateStr.charAt(0)) + Character.toString(dateStr.charAt(1));
		String monthNo = Character.toString(dateStr.charAt(3)) + Character.toString(dateStr.charAt(4));
		String year = Character.toString(dateStr.charAt(8)) + Character.toString(dateStr.charAt(9));
		int month = Integer.parseInt(monthNo);

		switch (month)
		{
			case 1:
				engMonth = "Jan";
				break;
			case 2:
				engMonth = "Feb";
				break;
			case 3:
				engMonth = "Mar";
				break;
			case 4:
				engMonth = "Apr";
				break;
			case 5:
				engMonth = "May";
				break;
			case 6:
				engMonth = "Jun";
				break;
			case 7:
				engMonth = "Jul";
				break;
			case 8:
				engMonth = "Aug";
				break;
			case 9:
				engMonth = "Sep";
				break;
			case 10:
				engMonth = "Oct";
				break;
			case 11:
				engMonth = "Nov";
				break;
			case 12:
				engMonth = "Dec";
				break;
		}
		int yearNo = Integer.parseInt(year);
		return dateNo + "-" + engMonth + "-" + yearNo;
	}

	public static int compareTime(String strTime1, String strTime2)
	{
		String arrTime1[] = strTime1.split(":");
		String arrTime2[] = strTime2.split(":");
		String strArrTime1 = arrTime1[0];
		String strArrTime2 = arrTime2[0];

		if (arrTime1[0].substring(0, 1).equalsIgnoreCase("0"))
		{
			strArrTime1 = arrTime1[0].substring(1, 1);
		}

		if (arrTime2[0].substring(0, 1).equalsIgnoreCase("0"))
		{
			strArrTime2 = arrTime2[0].substring(1, 1);
		}

		int intTime1 = Integer.parseInt(strArrTime1 + arrTime1[1] + arrTime1[2]);
		int intTime2 = Integer.parseInt(strArrTime2 + arrTime2[1] + arrTime2[2]);
		int diffTime = intTime1 - intTime2;
		return diffTime;
	}

	public static int compareDate(Date date1, Date date2, int returnType)
	{
		if (returnType == 0)
		{
			returnType = RETURN_TYPE_DAY;
		}
		int diffInDays = (int) ((date1.getTime() - date2.getTime()) / returnType);
		return diffInDays;
	}

	public static boolean compareDateBetween(Date dateStart, Date dateEnd, Date compareDate)
	{
		Calendar start = Calendar.getInstance(Locale.US);
		start.setTime(dateStart);
		Calendar end = Calendar.getInstance(Locale.US);
		end.setTime(dateEnd);
		Calendar now = Calendar.getInstance(Locale.US);
		now.setTime(compareDate);
		return start.before(now) && end.after(now);
	}


	public static BigDecimal generateIntIDByCurrentDateTimeInMillis() {
		Date dateIntID = getCurrentDateTimeInMillis();

		SimpleDateFormat sFormat = new SimpleDateFormat(DATE_INT_ID);
		String strDateIntId = sFormat.format(dateIntID);

		return new BigDecimal(strDateIntId);
	}


	public static Date getCurrentDateTimeInMillis()
	{
		Calendar now = Calendar.getInstance(Locale.US);
		return new Date(now.getTimeInMillis());
	}

	public static Date convertStringToDateRange(String strDate, Boolean StartDate, SimpleDateFormat format)
	{
		String time = "";
		if (StartDate)
		{
			time = " 00:00:00";
		}
		else
		{
			time = " 23:59:59";
		}
		strDate = strDate + time;
		Date date = null;
		SimpleDateFormat sFormat;
		if (format == null)
		{
			sFormat = new SimpleDateFormat(DEFAULT_TIME_STAMP_FORMAT, Locale.US);
		}
		else
		{
			sFormat = format;
		}

		if (!StringUtility.isEmpty(strDate))
		{
			try
			{
				// ADD
				String dateTime[] = strDate.split(" ");
				String strDates[] = dateTime[0].split("/");

				if (strDates != null && strDates.length == 3)
				{
					BigDecimal resultYear = new BigDecimal(strDates[strDates.length - 1]);
					resultYear = resultYear.subtract(new BigDecimal(543));
					if (strDates != null && strDates.length == 3 && resultYear.compareTo(new BigDecimal(1900)) == 1)
					{
						strDate = strDates[0] + "/" + strDates[1] + "/" + resultYear.toString() + " " + dateTime[1];
					}
				}
				// END ADD
				date = sFormat.parse(strDate);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return date;
	}

	public static boolean compareDate(Date date1, Date date2)
	{
		// date1 before or equals date2 >> True
		Calendar start = Calendar.getInstance(Locale.US);
		start.setTime(date1);
		Calendar end = Calendar.getInstance(Locale.US);
		end.setTime(date2);
		return start.before(end) || start.equals(end);
	}



	public static int diffDateHour(Date from, Date to)
	{
		long secs = (to.getTime() - from.getTime()) / 1000;
		long hours = secs / 3600;
		return (int) hours;
	}

	public static int diffDateDate(Date from, Date to)
	{
		return diffDateHour(from, to) / 24;
	}

}
