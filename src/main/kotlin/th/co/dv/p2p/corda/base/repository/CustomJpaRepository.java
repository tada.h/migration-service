/**
* <pre><b>
*	Version 	Date		Time		Developer	Remark</b>
*	001		Feb 19, 2014	2:13:38 PM	chatchch
* </pre>
*/
package th.co.dv.p2p.corda.base.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import th.co.dv.p2p.corda.base.utility.search.SearchCriterias;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author chatchch
 *
 */
@NoRepositoryBean
public interface CustomJpaRepository<T, ID extends Serializable> extends JpaRepository<T, ID>
{
	List<T> list(SearchCriterias criterias);
	List<T> list(SearchCriterias criterias, Map<String, Object> param);
	List<T> list(SearchCriterias criterias, Map<String, Object> param, Map<String, String> operation);
	int count(SearchCriterias criterias);
}
