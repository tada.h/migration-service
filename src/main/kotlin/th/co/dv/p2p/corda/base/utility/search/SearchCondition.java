/**
* <pre><b>
*	Version 	Date		Time		Developer	Remark</b>
*	001		Nov 26, 2010	3:00:04 PM	chatchch
* </pre>
*/
package th.co.dv.p2p.corda.base.utility.search;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

/**
 *
 */
public interface SearchCondition
{
	public Predicate createPredicate(CriteriaBuilder cb, Path<?> root);
	public boolean isAnd();
	public void setAnd(boolean and);
}
