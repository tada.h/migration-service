/**
* <pre><b>
*	Version 	Date		Time		Developer	Remark</b>
*	001		Apr 27, 2011	3:15:06 PM	chatchch
* </pre>
*/
package th.co.dv.p2p.corda.base.utility.search;

public enum SearchCriteriaOperation
{
	EQUAL(" = "),
	EQUAL_INSENSITIVE(" = "),
	NOT_EQUAL(" <> "),
	LESSTHAN(" < "),
	LESSTHAN_OR_EQUAL(" <= "),
	GREATERTHAN(" > "),
	GREATERTHAN_OR_EQUAL(" >= "),
	STARTS_WITH(" START "),
	ENDS_WITH(" END" ),
	CONTAIN(" CONTAIN "),
	IN(" IN "),
	NOT_IN(" NOT IN "),
	BETWEEN(" BETWEEN "),
	ISNULL(" IS NULL "),
	NOTNULL(" IS NOT NULL "),
	CONTAIN_INSENSITIVE(" CONTAIN INSENSITIVE "),
	STARTS_WITH_INSENSITIVE(" START INSENSITIVE "),
	ENDS_WITH_INSENSITIVE(" END INSENSITIVE ");

	private String realOp;
	SearchCriteriaOperation(String op)
	{
		this.realOp = op;
	}
	public String getRealOp()
	{
		return realOp;
	}

	public static SearchCriteriaOperation get(String name) {
		return EQUAL;
	}
}
