package th.co.dv.p2p.corda.base.utility.search;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;


/**
 *<pre>
 * <b>
 * Version 	Date		Time		Developer	Remark</b>
 * 001		Sep 30, 2009	2:21:48 PM	chatchch
 * </pre>
 */
public class SearchCriteria implements SearchCondition
{
	public static final String OP_EQUAL = " = ";
	public static final String OP_NOT_EQUAL = " <> ";
	public static final String OP_LESSTHAN = " < ";
	public static final String OP_LESSTHAN_OR_EQUAL = " <= ";
	public static final String OP_GREATERTHAN = " > ";
	public static final String OP_GREATERTHAN_OR_EQUAL = " >= ";
	public static final String OP_STARTS_WITH = " START ";
	public static final String OP_ENDS_WITH = " END ";
	public static final String OP_CONTAIN = " CONTAIN ";
	public static final String OP_IN = " IN ";
	public static final String OP_NOT_IN = " NOT IN ";
	public static final String OP_BETWEEN = " BETWEEN ";
	public static final String OP_CONTAIN_INSENSITIVE = " CONTAIN INSENSITIVE ";
	public static final String OP_STARTS_WITH_INSENSITIVE = " START INSENSITIVE ";
	public static final String OP_ENDS_WITH_INSENSITIVE = " END INSENSITIVE ";



	@SuppressWarnings("unused")
	private static Log logger = LogFactory.getLog(SearchCriteria.class);

	private String path;

	private String field;

	private String type;

	private SearchCriteriaOperation op = SearchCriteriaOperation.EQUAL;

	private Object value;

	private Object value2;

	private boolean and = true;

	/**
	 * @return the field
	 */
	public String getField()
	{
		return field;
	}

	/**
	 * @param field the field to set
	 */
	public void setField(String field)
	{
		this.field = field;
	}

	/**
	 * @return the value
	 */
	public Object getValue()
	{
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Object value)
	{
		this.value = value;
	}

	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type)
	{
		this.type = type;
	}

	/**
	 * @return the op
	 */
	public String getRealOp()
	{
		return op.getRealOp();
	}

	public SearchCriteriaOperation getOp()
	{
		return op;
	}

	/**
	 * @param op the op to set
	 */
	public void setOp(String op)
	{
		if (OP_EQUAL.equals(op))
			this.op = SearchCriteriaOperation.EQUAL;
		else if (OP_NOT_EQUAL.equals(op))
			this.op = SearchCriteriaOperation.NOT_EQUAL;
		else if (OP_LESSTHAN.equals(op))
			this.op = SearchCriteriaOperation.LESSTHAN;
		else if (OP_LESSTHAN_OR_EQUAL.equals(op))
			this.op = SearchCriteriaOperation.LESSTHAN_OR_EQUAL;
		else if (OP_GREATERTHAN.equals(op))
			this.op = SearchCriteriaOperation.GREATERTHAN;
		else if (OP_GREATERTHAN_OR_EQUAL.equals(op))
			this.op = SearchCriteriaOperation.GREATERTHAN_OR_EQUAL;
		else if (OP_STARTS_WITH.equals(op))
			this.op = SearchCriteriaOperation.STARTS_WITH;
		else if (OP_ENDS_WITH.equals(op))
			this.op = SearchCriteriaOperation.ENDS_WITH;
		else if (OP_CONTAIN.equals(op))
			this.op = SearchCriteriaOperation.CONTAIN;
		else if (OP_IN.equals(op))
			this.op = SearchCriteriaOperation.IN;
		else if (OP_NOT_IN.equals(op))
			this.op = SearchCriteriaOperation.NOT_IN;
		else if (OP_BETWEEN.equals(op))
			this.op = SearchCriteriaOperation.BETWEEN;
		else if (OP_CONTAIN_INSENSITIVE.equals(op))
			this.op = SearchCriteriaOperation.CONTAIN_INSENSITIVE;
		else
			this.op = SearchCriteriaOperation.EQUAL;

	}

	public void setOp(SearchCriteriaOperation op)
	{
		this.op = op;
	}

	public Predicate createPredicate(CriteriaBuilder cb, Path<?> root)
	{
		if (SearchCriteriaOperation.EQUAL.equals(op))
		{
			return createEqualPredicate(cb, root);
		}
		if (SearchCriteriaOperation.NOT_EQUAL.equals(op))
		{
			return createNotEqualPredicate(cb, root);
		}

		if (SearchCriteriaOperation.LESSTHAN.equals(op))
		{
			return createLessThanPredicate(cb, root);
		}
		if (SearchCriteriaOperation.LESSTHAN_OR_EQUAL.equals(op))
		{
			return createLessThanOrEqualPredicate(cb, root);
		}
		if (SearchCriteriaOperation.GREATERTHAN.equals(op))
		{
			return createGreaterThanPredicate(cb, root);
		}
		if (SearchCriteriaOperation.GREATERTHAN_OR_EQUAL.equals(op))
		{
			return createGreaterThanOrEqualPredicate(cb, root);
		}
		if (SearchCriteriaOperation.STARTS_WITH.equals(op) || SearchCriteriaOperation.ENDS_WITH.equals(op) || SearchCriteriaOperation.CONTAIN.equals(op))
		{
			return createLikePredicate(cb, root);
		}
		if (SearchCriteriaOperation.IN.equals(op))
		{
			return createInPredicate(cb, root);
		}
		if (SearchCriteriaOperation.NOT_IN.equals(op))
		{
			return createNotInPredicate(cb, root);
		}
		if (SearchCriteriaOperation.ISNULL.equals(op))
		{
			return createIsNullPredicate(cb, root);
		}
		if (SearchCriteriaOperation.NOTNULL.equals(op))
		{
			return createNotNullPredicate(cb, root);
		}
		if (SearchCriteriaOperation.BETWEEN.equals(op))
		{
			return createBetweenPredicate(cb, root);
		}
		if (SearchCriteriaOperation.STARTS_WITH_INSENSITIVE.equals(op) || SearchCriteriaOperation.ENDS_WITH_INSENSITIVE.equals(op) || SearchCriteriaOperation.CONTAIN_INSENSITIVE.equals(op))
		{
			return createLikeInsensitivePredicate(cb, root);
		}
		if (SearchCriteriaOperation.EQUAL_INSENSITIVE.equals(op))
		{
			return createEqualInsensitivePredicate(cb, root);
		}

		return null;
	}

	private Predicate createEqualInsensitivePredicate(CriteriaBuilder cb, Path<?> root)
	{
		return cb.equal(cb.lower(root.get(field).as(String.class)), value.toString().toLowerCase());
	}

	private Predicate createEqualPredicate(CriteriaBuilder cb, Path<?> root)
	{
		if (null == value)
		{
			return cb.isNull(root.get(field));
		}

		Class<?> c = root.get(field).getJavaType();
		if (c.getName().equals("boolean"))
		{
			// throw new UnsupportedOperationException("Less than operation is not support type: " + c.getName());
			return cb.equal(root.get(field).as(Boolean.TYPE), new Boolean(value.toString()));
		}	else if (c.getName().equals("java.lang.Boolean")) {
			return cb.equal(root.get(field).as(Boolean.class), new Boolean(value.toString()));
		} else if (c.getName().equals("long"))
		{
			return cb.equal(root.get(field).as(Long.TYPE), new Long(value.toString()));
		}
		else if (c.getName().equals("java.lang.Long"))
		{
			return cb.equal(root.get(field).as(Long.class), new Long(value.toString()));
		}
		else if (c.getName().equals("int"))
		{
			return cb.equal(root.get(field).as(Integer.TYPE), new Integer(value.toString()));
		}
		else if (c.getName().equals("java.lang.Integer"))
		{
			return cb.equal(root.get(field).as(Integer.class), new Integer(value.toString()));
		}
		else if (c.getName().equals("double"))
		{
			return cb.equal(root.get(field).as(Double.TYPE), new Double(value.toString()));
		}
		else if (c.getName().equals("java.lang.Double"))
		{
			return cb.equal(root.get(field).as(Double.class), new Double(value.toString()));
		}
		else if (c.getName().equals("java.math.BigDecimal"))
		{
			return cb.equal(root.get(field).as(BigDecimal.class), new BigDecimal(value.toString()));
		}
		else if (c.getName().equals("java.util.Date"))
		{
			if (value instanceof Date)
				return cb.equal(root.get(field).as(Date.class), (Date) value);
			return cb.equal(root.get(field).as(Date.class), parseDate(value.toString()));
		}

		// return cb.lessThan(root.get(field).as(String.class), value.toString());
		return cb.equal(root.get(field), value);
	}

	private Date parseDate(String value)
	{
		try {
			return (new SimpleDateFormat("dd/MM/yyyy")).parse(value.toString());
		} catch(ParseException ex) {
			return null;
		}
	}

	private Predicate createNotEqualPredicate(CriteriaBuilder cb, Path<?> root)
	{
		if (null == value)
		{
			return cb.isNotNull(root.get(field));
		}

		return cb.notEqual(root.get(field), value);
	}

	private Predicate createIsNullPredicate(CriteriaBuilder cb, Path<?> root)
	{
		return cb.isNull(root.get(field));
	}

	private Predicate createNotNullPredicate(CriteriaBuilder cb, Path<?> root)
	{
		return cb.isNotNull(root.get(field));
	}

	private Predicate createLessThanPredicate(CriteriaBuilder cb, Path<?> root)
	{
		if (null == value)
		{
			throw new UnsupportedOperationException("Cannot compare null with Less than operation.");
		}

		Class<?> c = root.get(field).getJavaType();
		if (c.getName().toLowerCase().contains("boolean"))
		{
			throw new UnsupportedOperationException("Less than operation is not support type: " + c.getName());
		}

		if (c.getName().equals("long"))
		{
			return cb.lessThan(root.get(field).as(Long.TYPE), new Long(value.toString()));
		}
		else if (c.getName().equals("java.lang.Long"))
		{
			return cb.lessThan(root.get(field).as(Long.class), new Long(value.toString()));
		}
		else if (c.getName().equals("int"))
		{
			return cb.lessThan(root.get(field).as(Integer.TYPE), new Integer(value.toString()));
		}
		else if (c.getName().equals("java.lang.Integer"))
		{
			return cb.lessThan(root.get(field).as(Integer.class), new Integer(value.toString()));
		}
		else if (c.getName().equals("double"))
		{
			return cb.lessThan(root.get(field).as(Double.TYPE), new Double(value.toString()));
		}
		else if (c.getName().equals("java.lang.Double"))
		{
			return cb.lessThan(root.get(field).as(Double.class), new Double(value.toString()));
		}
		else if (c.getName().equals("java.math.BigDecimal"))
		{
			return cb.lessThan(root.get(field).as(BigDecimal.class), new BigDecimal(value.toString()));
		}
		else if (c.getName().equals("java.util.Date"))
		{
			return cb.lessThan(root.get(field).as(Date.class), (Date) value);
		}

		return cb.lessThan(root.get(field).as(String.class), value.toString());
	}

	private Predicate createLessThanOrEqualPredicate(CriteriaBuilder cb, Path<?> root)
	{
		if (null == value)
		{
			throw new UnsupportedOperationException("Cannot compare null with Less than or equal operation.");
		}

		Class<?> c = root.get(field).getJavaType();
		if (c.getName().toLowerCase().contains("boolean"))
		{
			throw new UnsupportedOperationException("Less than or equal operation is not support type: " + c.getName());
		}

		if (c.getName().equals("long"))
		{
			return cb.lessThanOrEqualTo(root.get(field).as(Long.TYPE), new Long(value.toString()));
		}
		else if (c.getName().equals("java.lang.Long"))
		{
			return cb.lessThanOrEqualTo(root.get(field).as(Long.class), new Long(value.toString()));
		}
		else if (c.getName().equals("int"))
		{
			return cb.lessThanOrEqualTo(root.get(field).as(Integer.TYPE), new Integer(value.toString()));
		}
		else if (c.getName().equals("java.lang.Integer"))
		{
			return cb.lessThanOrEqualTo(root.get(field).as(Integer.class), new Integer(value.toString()));
		}
		else if (c.getName().equals("double"))
		{
			return cb.lessThanOrEqualTo(root.get(field).as(Double.TYPE), new Double(value.toString()));
		}
		else if (c.getName().equals("java.lang.Double"))
		{
			return cb.lessThanOrEqualTo(root.get(field).as(Double.class), new Double(value.toString()));
		}
		else if (c.getName().equals("java.math.BigDecimal"))
		{
			return cb.lessThanOrEqualTo(root.get(field).as(BigDecimal.class), new BigDecimal(value.toString()));
		}
		else if (c.getName().equals("java.util.Date"))
		{
			return cb.lessThanOrEqualTo(root.get(field).as(Date.class), (Date) value);
		}

		return cb.lessThanOrEqualTo(root.get(field).as(String.class), value.toString());
	}

	private Predicate createGreaterThanPredicate(CriteriaBuilder cb, Path<?> root)
	{
		if (null == value)
		{
			throw new UnsupportedOperationException("Cannot compare null with Greater than operation.");
		}

		Class<?> c = root.get(field).getJavaType();
		if (c.getName().toLowerCase().contains("boolean"))
		{
			throw new UnsupportedOperationException("Greater than operation is not support type: " + c.getName());
		}

		if (c.getName().equals("long"))
		{
			return cb.greaterThan(root.get(field).as(Long.TYPE), new Long(value.toString()));
		}
		else if (c.getName().equals("java.lang.Long"))
		{
			return cb.greaterThan(root.get(field).as(Long.class), new Long(value.toString()));
		}
		else if (c.getName().equals("int"))
		{
			return cb.greaterThan(root.get(field).as(Integer.TYPE), new Integer(value.toString()));
		}
		else if (c.getName().equals("java.lang.Integer"))
		{
			return cb.greaterThan(root.get(field).as(Integer.class), new Integer(value.toString()));
		}
		else if (c.getName().equals("double"))
		{
			return cb.greaterThan(root.get(field).as(Double.TYPE), new Double(value.toString()));
		}
		else if (c.getName().equals("java.lang.Double"))
		{
			return cb.greaterThan(root.get(field).as(Double.class), new Double(value.toString()));
		}
		else if (c.getName().equals("java.math.BigDecimal"))
		{
			return cb.greaterThan(root.get(field).as(BigDecimal.class), new BigDecimal(value.toString()));
		}
		else if (c.getName().equals("java.util.Date"))
		{
			return cb.greaterThan(root.get(field).as(Date.class), (Date) value);
		}

		return cb.greaterThan(root.get(field).as(String.class), value.toString());
	}

	private Predicate createGreaterThanOrEqualPredicate(CriteriaBuilder cb, Path<?> root)
	{
		if (null == value)
		{
			throw new UnsupportedOperationException("Cannot compare null with Greater than or equal than operation.");
		}

		Class<?> c = root.get(field).getJavaType();
		if (c.getName().toLowerCase().contains("boolean"))
		{
			throw new UnsupportedOperationException("Greater than or equal operation is not support type: " + c.getName());
		}

		if (c.getName().equals("long"))
		{
			return cb.greaterThanOrEqualTo(root.get(field).as(Long.TYPE), new Long(value.toString()));
		}
		else if (c.getName().equals("java.lang.Long"))
		{
			return cb.greaterThanOrEqualTo(root.get(field).as(Long.class), new Long(value.toString()));
		}
		else if (c.getName().equals("int"))
		{
			return cb.greaterThanOrEqualTo(root.get(field).as(Integer.TYPE), new Integer(value.toString()));
		}
		else if (c.getName().equals("java.lang.Integer"))
		{
			return cb.greaterThanOrEqualTo(root.get(field).as(Integer.class), new Integer(value.toString()));
		}
		else if (c.getName().equals("double"))
		{
			return cb.greaterThanOrEqualTo(root.get(field).as(Double.TYPE), new Double(value.toString()));
		}
		else if (c.getName().equals("java.lang.Double"))
		{
			return cb.greaterThanOrEqualTo(root.get(field).as(Double.class), new Double(value.toString()));
		}
		else if (c.getName().equals("java.math.BigDecimal"))
		{
			return cb.greaterThanOrEqualTo(root.get(field).as(BigDecimal.class), new BigDecimal(value.toString()));
		}
		else if (c.getName().equals("java.util.Date"))
		{
			return cb.greaterThanOrEqualTo(root.get(field).as(Date.class), (Date) value);
		}

		return cb.greaterThanOrEqualTo(root.get(field).as(String.class), value.toString());
	}

	private Predicate createLikePredicate(CriteriaBuilder cb, Path<?> root)
	{
		if (null == value)
		{
			return cb.isNull(root.get(field));
		}

		Class<?> c = root.get(field).getJavaType();
		if (!c.getName().toLowerCase().contains("string"))
		{
			throw new UnsupportedOperationException("Like operation is not support type: " + c.getName());
		}

		String v = value.toString();
		if (op.equals(SearchCriteriaOperation.STARTS_WITH) || op.equals(SearchCriteriaOperation.CONTAIN))
		{
			v = v + "%";

		}
		if (op.equals(SearchCriteriaOperation.ENDS_WITH) || op.equals(SearchCriteriaOperation.CONTAIN))
		{
			v = "%" + v;
		}

		return cb.like(root.get(field).as(String.class), v);
	}


	private Predicate createLikeInsensitivePredicate(CriteriaBuilder cb, Path<?> root)
	{
		if (null == value)
		{
			return cb.isNull(root.get(field));
		}

		Class<?> c = root.get(field).getJavaType();
		if (!c.getName().toLowerCase().contains("string"))
		{
			throw new UnsupportedOperationException("Like operation is not support type: " + c.getName());
		}

		String v = value.toString().toUpperCase();
		if (op.equals(SearchCriteriaOperation.STARTS_WITH_INSENSITIVE) || op.equals(SearchCriteriaOperation.CONTAIN_INSENSITIVE))
		{
			v = v + "%";

		}
		if (op.equals(SearchCriteriaOperation.ENDS_WITH_INSENSITIVE) || op.equals(SearchCriteriaOperation.CONTAIN_INSENSITIVE))
		{
			v = "%" + v;
		}

		return cb.like(cb.upper(root.get(field).as(String.class)), v);
	}

	private Predicate createInPredicate(CriteriaBuilder cb, Path<?> root)
	{
		if (!(value instanceof Collection<?>))
		{
			return createEqualPredicate(cb, root);
		}

		return cb.in(root.get(field)).value(value);
	}

	private Predicate createNotInPredicate(CriteriaBuilder cb, Path<?> root)
	{
		if (!(value instanceof Collection<?>))
		{
			return createNotEqualPredicate(cb, root);
		}

		return cb.in(root.get(field)).value(value).not();
	}

	private Predicate createBetweenPredicate(CriteriaBuilder cb, Path<?> root)
	{
		if (null == value)
		{
			throw new UnsupportedOperationException("Cannot compare null with Between operation.");
		}
		if (null == value2)
		{
			throw new UnsupportedOperationException("Cannot compare null with Between operation.");
		}

		if (value instanceof Date)
			return cb.between(root.get(field).as(Date.class), (Date)value, (Date)value2);

		return cb.between(root.get(field).as(BigDecimal.class), (BigDecimal)value, (BigDecimal)value2);
	}


	/**
	 * @return the path
	 */
	public String getPath()
	{
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path)
	{
		this.path = path;
	}

	/**
	 * @return the and
	 */
	public boolean isAnd()
	{
		return and;
	}

	/**
	 * @param and the and to set
	 */
	public void setAnd(boolean and)
	{
		this.and = and;
	}

	/**
	 * @return the value2
	 */
	public Object getValue2()
	{
		return value2;
	}

	/**
	 * @param value2 the value2 to set
	 */
	public void setValue2(Object value2)
	{
		this.value2 = value2;
	}
}
