package th.co.dv.p2p.corda.base.utility.search.paging;

import th.co.dv.p2p.corda.base.utility.search.SearchCriterias;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

public class PaginationHelper {

	@SuppressWarnings("unchecked")
	public static <T> PagableList<T> fetch(SearchCriterias criterias, int page, int pageSize, String orderBy, boolean asc, EntityManager entityManager){

		if(page > 0 && pageSize > 0){
			criterias.setPage(page);
			criterias.setPageSize(pageSize);

			if(orderBy != null && orderBy.length() > 0){
				criterias.addSort(orderBy, asc);
			}
		}

		Query query = criterias.createQuery(entityManager);

		PagableList<T> pList = new PagableList<T>(query.getResultList());

		if(page > 0 && pageSize > 0){
			TypedQuery<Long> pageQuery = (TypedQuery<Long>) criterias.createTotalQuery(entityManager);
			pList.setTotalSize(pageQuery.getSingleResult().intValue());
		}

		return pList;
	}
}
