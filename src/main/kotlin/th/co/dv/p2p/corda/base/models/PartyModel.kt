package th.co.dv.p2p.corda.base.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

/** For JSON serialisation */
// TODO: we may need to add more fields in the future for PartyModel, i.e locality, country.
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class PartyModel(

        @JsonProperty("legalName")
        var legalName: String? = null,
        @JsonProperty("organisation")
        var organisation: String? = null,
        @JsonProperty("organisationUnit")
        var organisationUnit: String? = null
)