/**
* <pre><b>
*	Version 	Date		Time		Developer	Remark</b>
*	001		Nov 26, 2010	3:20:36 PM	chatchch
* </pre>
*/
package th.co.dv.p2p.corda.base.utility.search;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.util.List;

/**
 *
 */
public class SearchConditionGroup implements SearchCondition
{
	private List<SearchCondition> searchConditions;

	private boolean and = false;

	/* (non-Javadoc)
	 * @see th.co.dv.p2p.corda.base.web.SearchCondition#createPredicate(javax.persistence.criteria.CriteriaBuilder, javax.persistence.criteria.Path)
	 */
	public Predicate createPredicate(CriteriaBuilder cb, Path<?> root)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the searchConditions
	 */
	public List<SearchCondition> getSearchConditions()
	{
		return searchConditions;
	}

	/**
	 * @param searchConditions the searchConditions to set
	 */
	public void setSearchConditions(List<SearchCondition> searchConditions)
	{
		this.searchConditions = searchConditions;
	}

	/**
	 * @return the and
	 */
	public boolean isAnd()
	{
		return and;
	}

	/**
	 * @param and the and to set
	 */
	public void setAnd(boolean and)
	{
		this.and = and;
	}
}
