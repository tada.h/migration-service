/* Copyright 2004, 2005, 2006 Acegi Technology Pty Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package th.co.dv.p2p.corda.base.utility.search.context;

import th.co.dv.p2p.corda.base.utility.search.SearchCriterias;

import java.io.Serializable;


/**
 * Interface defining the minimum Search information associated with the current
 * thread of execution.
 * <p>
 * The Search context is stored in a {@link SearchContextHolder}.
 * </p>
 *
 * @author Ben Alex
 * @version $Id: SearchContext.java,v 1.1 2017/11/08 03:15:40 wittawai Exp $
 */
public interface SearchContext extends Serializable
{
	// ~ Methods
	// ========================================================================================================

	/**
	 * Obtains the currently authenticated principal, or an authentication
	 * request token.
	 *
	 * @return the <code>Authentication</code> or <code>null</code> if no
	 *         authentication information is available
	 */
	SearchCriterias getSearchCriterias();

	/**
	 * Changes the currently authenticated principal, or removes the
	 * authentication information.
	 *
	 * @param authentication the new <code>Authentication</code> token, or
	 *        <code>null</code> if no further authentication information should
	 *        be stored
	 */
	void setSearchCriterias(SearchCriterias searchCriterias);
}
