/* Copyright 2004, 2005, 2006 Acegi Technology Pty Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package th.co.dv.p2p.corda.base.utility.search.context;

import th.co.dv.p2p.corda.base.utility.search.SearchCriterias;


/**
 * Base implementation of {@link SearchContext}.
 * <p>
 * Used by default by {@link SearchContextHolder} and
 * {@link HttpSessionContextIntegrationFilter}.
 * </p>
 *
 * @author Ben Alex
 * @version $Id: SearchContextImpl.java,v 1.1 2017/11/08 03:15:40 wittawai Exp $
 */
public class SearchContextImpl implements SearchContext
{
	private static final long serialVersionUID = -1594226511936935777L;

	// ~ Instance fields
	// ================================================================================================

	private SearchCriterias searchCriterias;

	// ~ Methods
	// ========================================================================================================

	public boolean equals(Object obj)
	{
		if (obj instanceof SearchContextImpl)
		{
			SearchContextImpl test = (SearchContextImpl) obj;

			if ((this.getSearchCriterias() == null) && (test.getSearchCriterias() == null))
			{
				return true;
			}

			if ((this.getSearchCriterias() != null) && (test.getSearchCriterias() != null) && this.getSearchCriterias().equals(test.getSearchCriterias()))
			{
				return true;
			}
		}

		return false;
	}

	public SearchCriterias getSearchCriterias()
	{
		return searchCriterias;
	}

	public int hashCode()
	{
		if (this.searchCriterias == null)
		{
			return -1;
		}

		return this.searchCriterias.hashCode();
	}

	public void setSearchCriterias(SearchCriterias searchCriterias)
	{
		this.searchCriterias = searchCriterias;
	}

	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(super.toString());

		if (this.searchCriterias == null)
		{
			sb.append(": Null searchCriterias");
		}
		else
		{
			sb.append(": SearchCriterias: ").append(this.searchCriterias);
		}

		return sb.toString();
	}
}
