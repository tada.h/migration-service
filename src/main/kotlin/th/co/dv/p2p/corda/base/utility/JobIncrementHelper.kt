package th.co.dv.p2p.corda.base.utility

import org.springframework.batch.core.JobParameters
import org.springframework.batch.core.JobParametersBuilder
import org.springframework.batch.core.JobParametersIncrementer
import java.util.*

/**
 *
 * Reference Document https://docs.spring.io/spring-batch/trunk/reference/htmlsingle/#JobParametersIncrementer
 *
 * This function will add default parameter RUN_DATE which has value = {@currentDate} to every job.
 */
class JobIncrementHelper: JobParametersIncrementer {
    override fun getNext(parameters: JobParameters?): JobParameters {
        return JobParametersBuilder().addDate("RUN_DATE", Date()).toJobParameters()
    }
}