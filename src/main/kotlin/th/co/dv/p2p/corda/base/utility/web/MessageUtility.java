package th.co.dv.p2p.corda.base.utility.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Locale;

@Component
public class MessageUtility
{
	public static final String RESULT_MESSAGE_KEY = "resultMessage";
	public static final String RESULT_MESSAGE_POPUP_KEY = "resultMessagePopup";
	public static final String ERROR_MESSAGE_POPUP_KEY = "errorMessagePopup";
	public static final String ERROR_MESSAGE_KEY = "errorMessage";
	public static final String SUCCESS_FLAG = "S";
	public static final String WARNING_FLAG = "W";
	public static final String ERROR_FLAG = "E";
	public static final String RERUN_FLAG = "R";
	public static final String BUSINESS_FLAG = "B";

	@Autowired
	protected MessageSource messageSource;

		public void addError(String msg)
	{
		addError(msg, false);
	}

	public void addError(String msg, boolean popup)
	{
		String key  = ERROR_MESSAGE_KEY;
		if (popup)
		{
			key = ERROR_MESSAGE_POPUP_KEY;
		}
		addMessageToModel(msg, key);
	}

	public void addError(String msg, Object[] arg)
	{
		addError(msg, arg, false);
	}

	public void addError(String msg, Object[] arg, boolean popup)
	{
		String realMsg = getLocaleMessage(msg, arg);
		addError(realMsg, popup);
	}

	public void addMessage(String msg)
	{
		addMessage(msg, false);
	}

	public void addMessage(String msg, boolean popup)
	{
		String key  = RESULT_MESSAGE_KEY;
		if (popup)
		{
			key = RESULT_MESSAGE_POPUP_KEY;
		}
		addMessageToModel(msg, key);
	}

	public void addMessage(String msg, Object[] arg)
	{
		addMessage(msg, arg, false);
	}

	public void addMessage(String msg, Object[] arg, boolean popup)
	{
		String realMsg = getLocaleMessage(msg, arg);
		addMessage(realMsg, popup);
	}

	public String getLocaleMessage(String key)
	{
		return getLocaleMessage(key, null);
	}

	public String getLocaleMessage(String key, Object[] args)
	{
		String realMsg = key;
		try
		{
			realMsg = messageSource.getMessage(key, args, LocaleContextHolder.getLocale());
		}
		catch (NoSuchMessageException e)
		{
		}
		return realMsg;
	}

	public String getHavingLocaleMessage(String key)
	{
		String realMsg = null;
		try
		{
			realMsg = messageSource.getMessage(key, null, LocaleContextHolder.getLocale());
		}
		catch (NoSuchMessageException e)
		{
		}
		return realMsg;
	}


	public String getMessageByLocale(String key,Locale locale)
	{
		String realMsg = null;
		try
		{
			realMsg = messageSource.getMessage(key, null,locale);
		}
		catch (NoSuchMessageException e)
		{
		}
		return realMsg;
	}



	@SuppressWarnings("unchecked")
	private void addMessageToModel(String msg, String key)
	{
		ArrayList<String> msgs = (ArrayList<String>) RequestUtility.getCurrentSession().getAttribute(key);
		if (null == msgs)
		{
			msgs = new ArrayList<String>();
		}
		msgs.add(msg);
		RequestUtility.getCurrentSession().setAttribute(key, msgs);
	}

	/**
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource)
	{
		this.messageSource = messageSource;
	}
}
