package th.co.dv.p2p.corda.base.utility.search.paging;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.List;


public class ListSerializer extends JsonSerializer<List> {

    private static final long serialVersionUID = 9024326729987814606L;

    @Override
    public void serialize(
        List value, JsonGenerator jgen, SerializerProvider provider)
      throws IOException, JsonProcessingException {

        if (value instanceof PagableList) {
            PagableList pList = (PagableList) value;
            jgen.writeStartObject();
            jgen.writeObjectField("rows", pList.getData());
            jgen.writeNumberField("page", pList.getPage());
            jgen.writeNumberField("pageSize", pList.getPageSize());
            jgen.writeNumberField("totalRecords", pList.getTotalSize());
            jgen.writeEndObject();
        } else {
            jgen.writeStartArray();
            for(Object obj : value)
                jgen.writeObject(obj);
            jgen.writeEndArray();
        }
    }
}