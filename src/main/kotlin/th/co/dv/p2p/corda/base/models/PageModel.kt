package th.co.dv.p2p.corda.base.models

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Returned in queries
 * A PageModel contains:
 *  1) [page]: The current Page number which the results are on
 *  2) [rows]: The actual records in current page
 *  3) [pageSize]: The count of [rows]
 *  4) [totalRecords]: The total records available matching the criteria to allow you to further paginate accordingly
 *
 *  TODO: need to rename [totalRecords] to totalRecordsAvailable to better reflect its purpose.
 *        Not to confuse with [pageSize] which is the total records in current page
 *
 *  For example, if [pageSize] based on actual number of [rows] is 200, and [totalRecords] is 208
 *  the API user may want to send another query for pageNumber #2 to get the remaining 8 records
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)

data class PageModel<T>(

        var page: Int = 1,

        var pageSize: Int,

        var totalRecordsAvailable: Long,

        var rows: List<T>) {

    @JsonCreator
    constructor(@JsonProperty("page") page: Int, @JsonProperty("rows") rows: List<T>, @JsonProperty("totalRecords") totalRecordsAvailable: Long) : this(
            page = page,
            pageSize = rows.size,
            totalRecordsAvailable = totalRecordsAvailable,
            rows = rows)
}