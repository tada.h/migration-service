/**
 * <pre><b>
 *	Version 	Date		Time		Developer	Remark</b>
 *	001		Nov 18, 2010	8:48:48 AM	chatchch
 * </pre>
 */

package th.co.dv.p2p.corda.base.utility;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.io.IOException;

/**
 *
 */
public class EncryptionUtility
{
	private static final char[] PASSWORD = "enfldsgbnlsngdlksdsgm".toCharArray();

	private static final byte[] SALT = {
		(byte) 0xde, (byte) 0x33, (byte) 0x10, (byte) 0x12,
		(byte) 0xde, (byte) 0x33, (byte) 0x10, (byte) 0x12,};

	public static String encrypt(String source)
	{
		try
		{
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
			SecretKey key = keyFactory.generateSecret(new PBEKeySpec(PASSWORD));
			Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
			pbeCipher.init(Cipher.ENCRYPT_MODE, key, new PBEParameterSpec(SALT, 20));
			return base64Encode(pbeCipher.doFinal(source.getBytes()));
		}
		catch (Exception e)
		{
			return source;
		}
	}

	private static String base64Encode(byte[] bytes)
	{
		return new String(bytes);
		// return new Base64Encoder().encode(bytes);
	}

	public static String decrypt(String source)
	{
		try
		{
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
			SecretKey key = keyFactory.generateSecret(new PBEKeySpec(PASSWORD));
			Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
			pbeCipher.init(Cipher.DECRYPT_MODE, key, new PBEParameterSpec(SALT, 20));
			String decryptedData = "";
			if ((source != null) && (!source.equals("")))
			{
				decryptedData = new String(pbeCipher.doFinal(base64Decode(source)));
			}
			return decryptedData;
		}
		catch (Exception e)
		{
			return source;
		}
	}

	private static byte[] base64Decode(String property) throws IOException
	{
		return property.getBytes();// new BASE64Decoder().decodeBuffer(property);
		// return new BASE64Decoder().decodeBuffer(property);
	}
}
